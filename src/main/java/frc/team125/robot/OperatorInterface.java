package frc.team125.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import frc.team125.robot.utils.*;


class OperatorInterface {
	private static final double STICK_DEADBAND = 0.01;
	private final Joystick driverPad;
	private final Joystick opPad;
	NutButton driveAssisted2;
	// Driver normal buttons
	NutButton intakeUntil;
	NutButton score;
	NutButton intakeNormal;
	NutButton forceCargo;
	NutButton forceHatch;
	NutButton turn180;
	NutButton turnNeg180;
	// Climber buttons
	NutButton prepClimb;
	// Operator NutButtons
	NutButton zero;
	NutButton horizontalFront;
	NutButton horizontalBack;
	NutButton mid;
	NutButton top;
	NutButton intakePsHatchFront;
	NutButton intakePsCargoFront;
	NutButton intakeGndCargo;
	NutButton intakePsHatchBack;
	NutButton intakePsCargoBack;
	NutButton cargoShipFront;
	NutButton cargoShipBack;
	NutButton forceFront;
	NutButton forceBack;
	NutButton forceClimb;
	NutButton forceRetract;
	private PeriodicCaller controllerUpdater;


	/**
	 * Contains all the buttons and initializes commands for the driver gamepads.
	 */
	OperatorInterface() {
		// These are the nut buttons
		// These are the buttons for when everything sucks with the launchpad
		this.driverPad = new Joystick(0);
		driveAssisted2 = new NutButton(new JoystickButton(driverPad, JoystickMap.B));
		//normal buttons
		intakeUntil = new NutButton(new JoystickButton(this.driverPad, JoystickMap.LB));
		score = new NutButton(new JoystickButton(this.driverPad, JoystickMap.RB));
		intakeNormal = new NutButton(new JoystickButton(this.driverPad, JoystickMap.Y));
		forceCargo = new NutButton(new JoystickButton(this.driverPad, JoystickMap.START));
		forceHatch = new NutButton(new JoystickButton(this.driverPad, JoystickMap.BACK));
		turn180 = new NutButton(new NutPov(this.driverPad, 135, 45));
		turnNeg180 = new NutButton(new NutPov(this.driverPad, 315, 225));

		opPad = new Joystick(1);
		this.horizontalFront = new NutButton(new JoystickButton(opPad, JoystickMap.A));
		this.horizontalBack = new NutButton(new JoystickButton(opPad, JoystickMap.A));
		this.mid = new NutButton(new JoystickButton(opPad, JoystickMap.B));
		this.top = new NutButton(new JoystickButton(opPad, JoystickMap.Y));

		this.intakePsHatchFront = new NutButton(new TriggerButton(opPad, JoystickMap.RIGHT_TRIGGER));
		this.intakePsHatchBack = new NutButton(new TriggerButton(opPad, JoystickMap.LEFT_TRIGGER));
		this.intakePsCargoFront = new NutButton(new JoystickButton(opPad, JoystickMap.RB));
		this.intakePsCargoBack = new NutButton(new JoystickButton(opPad, JoystickMap.LB));

		this.cargoShipFront = new NutButton(new JoystickButton(opPad, JoystickMap.R3));
		this.cargoShipBack = new NutButton(new JoystickButton(opPad, JoystickMap.L3));
		this.zero = new NutButton(new JoystickButton(opPad, JoystickMap.X));

		this.intakeGndCargo = new NutButton(new NutPov(opPad, 225, 135));
		this.forceFront = new NutButton(new JoystickButton(opPad, JoystickMap.START));
		this.forceBack = new NutButton(new JoystickButton(opPad, JoystickMap.BACK));

		this.prepClimb = new NutButton(new NutPov(this.driverPad, 225, 135));

		this.forceClimb = new NutButton(new JoystickButton(this.driverPad, JoystickMap.R3));
		this.forceRetract = new NutButton(new JoystickButton(this.driverPad, JoystickMap.A));
	}

	@SuppressWarnings("SameParameterValue")
	private static double stickDeadband(double value, double deadband, double center) {
		return (value < (center + deadband) && value > (center - deadband)) ? center : value;
	}

	double getDriverLeftStickX() {
		return stickDeadband(this.driverPad.getRawAxis(JoystickMap.LEFT_X), STICK_DEADBAND, 0.0);
	}

	double getDriverTriggerSum() {
		return this.driverPad.getRawAxis(JoystickMap.RIGHT_TRIGGER)
				- this.driverPad.getRawAxis(JoystickMap.LEFT_TRIGGER);
	}

	/**
	 * Get's the driver controller which should be at port 0
	 *
	 * @return driver's {@link Controller}
	 */
	public Joystick getDriverController() {
		return driverPad;
	}

	/**
	 * Get's the operator controller which should be at port 1
	 *
	 * @return operator's {@link Controller}
	 */
	public Joystick getOperatorController() {
		return opPad;
	}

	/**
	 * Updates controller's type
	 */
	public void updateControllers() {
//		getDriverController().updateControllerType();
//		getOperatorController().updateControllerType();
	}

	/**
	 * Starts updating the controllers' types
	 */
	public void startPeriodicControllerUpdate() {
		controllerUpdater = controllerUpdater == null ? new PeriodicCaller(1000, this::updateControllers) : controllerUpdater;
		try {
			if (!controllerUpdater.isAlive())
				controllerUpdater.start();
		} catch (IllegalThreadStateException ignored) {
		}
	}

	/**
	 * Stops updating the controllers' types
	 */
	public void stopPeriodicControllerUpdate() {
		controllerUpdater = controllerUpdater == null ? new PeriodicCaller(1000, this::updateControllers) : controllerUpdater;
		try {
			if (controllerUpdater.isAlive())
				controllerUpdater.interrupt();
		} catch (IllegalThreadStateException ignored) {
		}
	}

}


