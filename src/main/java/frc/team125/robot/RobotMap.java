package frc.team125.robot;

public class RobotMap {
	public static final int WRIST_MOTOR = 10;
	public static final int PIVOT_MOTOR = 14;
	public static final int ARM_MOTOR = 16;
	public static final int CARGO_INTAKE_MOTOR = 4;
	//Drivetrain
	public static final int LEFT_DRIVE_MAIN = 1;
	public static final int RIGHT_DRIVE_MAIN = 6;
	public static final int LEFT_DRIVE_FOLLOWER_A = 2;
	public static final int RIGHT_DRIVE_FOLLOWER_A = 5;
	// Intake wheels
	public static final int INTAKE_MOTOR = 3;
	//Climber
	public static final int CLIMBER_RIGHT = 8;
	public static final int CLIMBER_LEFT = 7;
	public static final int CLIMBER_SOLENOID = 2;
	// Solenoid
	public static final String LIMELIGHT_BACK = "limelight-bcam";
	public static final String LIMELIGHT_FRONT = "limelight";
	public static final int INTAKE_TOGGLE_SOLENOID = 1;
	public static final int CARGO_SOLENOID = 0;
}
