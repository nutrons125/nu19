package frc.team125.robot;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.buttons.NetworkButton;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.subsystems.*;
import frc.team125.robot.utils.Debouncer;
import frc.team125.robot.utils.JoystickMap;


/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
	private static final DriverStation driverStation = DriverStation.getInstance();
	private static final Vision vision = new Vision();
	private static final Superstructure superstructure = new Superstructure();
	private static final Compressor compressor = new Compressor(PneumaticsModuleType.CTREPCM);
	public static Drivetrain dt = new Drivetrain();
	private static OperatorInterface oi;
	private final Debouncer midDebouncer = new Debouncer(1.0);
	private final Debouncer highDebouncer = new Debouncer(1.0);
	private NetworkButton lForceFront;
	private NetworkButton lForceBack;
	private NetworkButton lZero;
	private NetworkButton lLowScore;
	private NetworkButton lMidScore;
	private NetworkButton lCSFront;
	private NetworkButton lCSBack;
	private NetworkButton lHighScore;
	private NetworkButton lGroundIntake;
	private NetworkButton lFrontHatchIntake;
	private NetworkButton lFrontCargoIntake;
	private NetworkButton lBackHatchIntake;
	private NetworkButton lBackCargoIntake;
	private NetworkButton lSuperHatch;
	private boolean globalFront = true;
	private boolean climbLimitDrive = false;
	private double desiredAngle = 0;


	/**
	 * This function is run when the robot is first started up and should be used for any
	 * initialization code.
	 */
	@Override
	public void robotInit() {
		compressor.enableDigital();

		vision.updateVisionDashboard();
		SendableChooser autoChooser = new SendableChooser<>();
		// autoChooser.addDefault("Default Wait Command", )


		SmartDashboard.putData("Auto Chooser 5000", autoChooser);

		vision.updateVisionDashboard();

		compressor.enableDigital();
		// This needs to be last since it initializes the starting teleop commands and everything else
		// needs to be initialized first
		oi = new OperatorInterface();

		NetworkTable table = NetworkTableInstance.getDefault().getTable("Launchpad");
		lForceFront = new NetworkButton(table, JoystickMap.LFORCE_FRONT);
		lForceBack = new NetworkButton(table, JoystickMap.LFORCE_BACK);
		lZero = new NetworkButton(table, JoystickMap.LZERO);
		lLowScore = new NetworkButton(table, JoystickMap.LLOW_SCORE);
		lMidScore = new NetworkButton(table, JoystickMap.LMID_SCORE);
		lCSFront = new NetworkButton(table, JoystickMap.LCS_FRONT);
		lCSBack = new NetworkButton(table, JoystickMap.LCS_BACK);
		lHighScore = new NetworkButton(table, JoystickMap.LHIGH_SCORE);
		@SuppressWarnings("unused")
		NetworkButton lDunk = new NetworkButton(table, JoystickMap.LDUNK);
		lGroundIntake = new NetworkButton(table, JoystickMap.LGROUND_INTAKE);
		lFrontHatchIntake = new NetworkButton(table, JoystickMap.LFRONT_HATCH_INTAKE);
		lFrontCargoIntake = new NetworkButton(table, JoystickMap.LFRONT_CARGO_INTAKE);
		lBackHatchIntake = new NetworkButton(table, JoystickMap.LBACK_HATCH_INTAKE);
		lBackCargoIntake = new NetworkButton(table, JoystickMap.LBACK_CARGO_INTAKE);
		lSuperHatch = new NetworkButton(table, JoystickMap.LSUPER_HATCH_INTAKE);
//        oi.startPeriodicControllerUpdate();
	}

	/**
	 * This function is called every robot packet, no matter the mode. Use this for items like
	 * diagnostics that you want ran during disabled, autonomous, teleoperated and test. This runs
	 * after the mode specific periodic functions, but before LiveWindow and SmartDashboard integrated
	 * updating.
	 */
	@Override
	public void robotPeriodic() {
		SmartDashboard.putBoolean("Pressure Switch", compressor.getPressureSwitchValue());
	}

	/**
	 * This autonomous (along with the chooser code above) shows how to select between different
	 * autonomous modes using the dashboard. The sendable chooser code works with the Java
	 * SmartDashboard. If you prefer the LabVIEW Dashboard, remove all of the chooser code and
	 * uncomment the getString line to get the auto name from the text box below the Gyro You can add
	 * additional auto modes by adding additional comparisons to the switch structure below with
	 * additional strings. If using the SendableChooser make sure to add them to the chooser code
	 * above as well.
	 */
	@Override
	public void autonomousInit() {
		this.teleopInit();
	}

	@Override
	public void autonomousPeriodic() {
		this.teleopPeriodic();
	}

	@Override
	public void disabledInit() {
		dt.updateSmartDashboard();
		dt.enableBrakeMode();
		superstructure.initialize();
		superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.IDLE);
		this.climbLimitDrive = false;

		if (DriverStation.isFMSAttached()) {
			int pivotPosToHold = (int) superstructure.pivot.getEncoderClicks();
			int armPosToHold = superstructure.arm.getEncoderClicks();
			int wristPosToHold = (int) superstructure.wrist.getEncoderClicks();
			superstructure.setGoal(new SuperstructureGoal(pivotPosToHold, armPosToHold, wristPosToHold));
		}
		oi.startPeriodicControllerUpdate();
	}

	@Override
	public void disabledPeriodic() {
		dt.updateSmartDashboard();
		vision.updateVisionDashboard();
		superstructure.updateSmartDashboard();

		SmartDashboard.putNumber("Controller 1 Rumble", 0);
//        oi.getOperatorController().rumble(SmartDashboard.getNumber("Controller 1 Rumble", 0));


		if (superstructure.isArmForward()) {
			vision.setFrontCamFeed();
		} else {
			vision.setRearCamFeed();
		}

		if (DriverStation.isFMSAttached()) {
			superstructure.update();
		}
	}

	@Override
	public void teleopInit() {
		dt.enableCoastMode();
		oi.stopPeriodicControllerUpdate();
	}

	@Override
	public void teleopPeriodic() {
//        compressor.clearAllPCMStickyFaults();
		runRobot();
	}

	/**
	 * This is the method that runs all of the robot code for teleop (and sandstorm).
	 */
	private void runRobot() {
//        oi.getDriverController().updateShuffleboard();
//        oi.getOperatorController().updateShuffleboard();
		dt.updateSmartDashboard();
		superstructure.updateSmartDashboard();
		vision.updateVisionDashboard();

		boolean globalHatchState = superstructure.acquiringHatch;

		// All the buttons
		if (oi.turn180.isClicked()) {
			dt.resetGyro();
			desiredAngle = dt.getAngle() + 135;
		}
		if (oi.turnNeg180.isClicked()) {
			dt.resetGyro();
			desiredAngle = dt.getAngle() - 135;
		}
		if (oi.forceClimb.isPressed()) {
			superstructure.climber.setPower(true);
		} else if (oi.forceRetract.isPressed()) {
			superstructure.climber.setPower(false);
		} else {
			superstructure.climber.setState(Climber.ClimberState.IDLE);
		}

		if (oi.prepClimb.isPressed()) {
			superstructure.setGoal(Constants.PREP_CLIMBING);
			superstructure.climber.setFingers(true);
			this.climbLimitDrive = true;
		}

		if (oi.forceFront.isPressed() || lForceFront.get()) {
			this.globalFront = true;
		} else if (oi.forceBack.isPressed() || lForceBack.get()) {
			this.globalFront = false;
		}

		if (oi.zero.isPressed() || lZero.get()) {
			superstructure.setGoal(Constants.ZERO);
		} else if ((oi.horizontalFront.isPressed() && this.globalFront) || (lLowScore.get() && this.globalFront)) {
			if (globalHatchState) {
				superstructure.setGoal(Constants.SCORE_HATCH_LOW_FRONT);
			} else {
				superstructure.setGoal(Constants.SCORE_CARGO_LOW_FRONT);
			}
		} else if (oi.top.isPressed() || lHighScore.get()) {
			superstructure.setGoal(Constants.HIGH_SCORING.getGoal(globalHatchState,
					this.highDebouncer.get(), this.globalFront));
		} else if (oi.mid.isPressed() || lMidScore.get()) {
			superstructure.setGoal(Constants.MID_SCORING.getGoal(globalHatchState,
					this.midDebouncer.get(), this.globalFront));
		} else if ((oi.horizontalBack.isPressed() && !this.globalFront) || (lLowScore.get() && !this.globalFront)) {
			if (globalHatchState) {
				superstructure.setGoal(Constants.SCORE_HATCH_LOW_BACK);
			} else {
				superstructure.setGoal(Constants.SCORE_CARGO_LOW_BACK);
			}
		} else if ((oi.intakeGndCargo.isPressed() || lGroundIntake.get()) && this.globalFront) {
			superstructure.acquiringHatch = false;
			superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.RUN_UNTIL);
			superstructure.setGoal(Constants.GROUND_CARGO_INTAKE);
		} else if ((oi.intakeGndCargo.isPressed() || this.lGroundIntake.get()) && !this.globalFront) {
			superstructure.acquiringHatch = false;
			superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.RUN_UNTIL);
			superstructure.setGoal(Constants.BACK_GROUND_CARGO);
		} else if (oi.cargoShipFront.isPressed() || lCSFront.get()) {
			this.globalFront = true;
			superstructure.acquiringHatch = false;
			superstructure.setGoal(Constants.CARGO_SHIP_FRONT); // Comment/delete this to get reach positions

			// TODO Uncomment this for the reach positions and uncomment the other setGoal
			//this.superstructure.setGoal(Constants.CARGO_SCORING.getGoal(this.globalHatchState,
			//        this.cargoDebouncer.get(), this.globalFront));
		} else if (oi.cargoShipBack.isPressed() || lCSBack.get()) {
			this.globalFront = false;
			superstructure.acquiringHatch = false;
			superstructure.setGoal(Constants.CARGO_SHIP_BACK); // Comment/delete this to get reach positions

			// TODO Uncomment this for the reach positions and uncomment the otehr setGoal
			//this.superstructure.setGoal(Constants.CARGO_SCORING.getGoal(this.globalHatchState,
			//        this.cargoDebouncer.get(), this.globalFront));
			// INTAKING BUTTONS
		} else if (oi.intakePsCargoFront.isPressed() || lFrontCargoIntake.get()) {
			this.globalFront = true;
			superstructure.acquiringHatch = false;
			superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.RUN_UNTIL);
			superstructure.setGoal(Constants.HP_CARGO_INTAKE_FRONT);
		} else if (oi.intakePsHatchFront.isPressed() || lFrontHatchIntake.get()) {
			this.globalFront = true;
			superstructure.acquiringHatch = true;
			superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.RUN_UNTIL);
			superstructure.setGoal(Constants.HP_HATCH_INTAKE_FRONT);
		} else if (oi.intakePsCargoBack.isPressed() || lBackCargoIntake.get()) {
			this.globalFront = false;
			superstructure.acquiringHatch = false;
			superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.RUN_UNTIL);
			superstructure.setGoal(Constants.HP_CARGO_INTAKE_BACK);
		} else if (oi.intakePsHatchBack.isPressed() || lBackHatchIntake.get()) {
			this.globalFront = false;
			superstructure.acquiringHatch = true;
			superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.RUN_UNTIL);
			superstructure.setGoal(Constants.HP_HATCH_INTAKE_BACK);
		} else if (lSuperHatch.get()) {
			superstructure.acquiringHatch = true;
			superstructure.setGoal(Constants.HP_SUPER_HATCH_INTAKE);
		}

		// Wrist Intake Buttons
		if (oi.intakeUntil.isPressed()) {
			superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.RUN_UNTIL);
		} else if (oi.intakeNormal.isPressed()) {
			superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.RUN);
		} else if (oi.score.isPressed()) {
			superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.SCORING);
		} else if (oi.getDriverController().getRawButtonReleased(JoystickMap.Y)
				|| oi.getDriverController().getRawButtonReleased(JoystickMap.RB)
				|| oi.getDriverController().getRawButtonReleased(JoystickMap.R3)) {
			superstructure.setWristIntakeState(IntakeGoal.WristIntakeState.IDLE);
		}

		// Override game piece buttons
		if (oi.
				forceCargo.isPressed()) {
			superstructure.acquiringHatch = false;
		} else if (oi.forceHatch.isPressed()) {
			superstructure.acquiringHatch = true;
		}

		if (superstructure.justAcquiredGamePiece()) {
			vision.flash();
		} else {
			// Update which limelight is used
			if (this.globalFront) {
				vision.setFrontCamFeed();
			} else {
				vision.setRearCamFeed();
			}
		}

		// TODO Make sure this drives/doesn't drive when we expect it to
		dt.update(this.climbLimitDrive,
				oi.driveAssisted2.isPressed(),
				oi.getDriverTriggerSum(),
				oi.getDriverLeftStickX(),
				vision.getXOffset(),
				oi.turn180.isPressed() || oi.turnNeg180.isPressed(),
				desiredAngle);
		superstructure.update();

		this.midDebouncer.update(oi.mid.isPressed());
		this.highDebouncer.update(oi.top.isPressed());
	}
}
