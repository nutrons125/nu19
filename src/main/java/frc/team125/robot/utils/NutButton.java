package frc.team125.robot.utils;

import edu.wpi.first.wpilibj.buttons.Button;

public class NutButton {

	private final Button nuttuButtu;
	private Boolean lastButtState = false;

	public NutButton(Button nuttuButtu) {
		this.nuttuButtu = nuttuButtu;
	}

	/**
	 * Checks to see if a NutButton has been clicked.
	 *
	 * @return If button is clicked.
	 */
	public boolean isClicked() {
		boolean retVal = nuttuButtu.get() && !lastButtState;
		this.lastButtState = this.nuttuButtu.get();
		return retVal;
	}

	/**
	 * Checks to see if the button has been pressed.
	 *
	 * @return If button is pressed.
	 */
	public boolean isPressed() {
		return this.nuttuButtu.get();
	}

}
