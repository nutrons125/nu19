package frc.team125.robot.utils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;



/*
 * Wish List:
 *  - addKey(String key) -adds a new column to an already written to file and syncronously fills
 *  in the column with a default value up to the current row
 *  - setDefault(String key, Object value) - sets a default value so that if a certain field
 *  isn't updated with the others it gets filled in
 *  - different levels of verbose-ness for logging
 */


public class CsvWriter {
	private final HashMap<String, Object> currentFields = new LinkedHashMap<>();
	private final ConcurrentLinkedDeque<String> linesToWrite = new ConcurrentLinkedDeque<>();
	private final LogTimer timer;
	private PrintWriter fileWriter;

	/**
	 * Creates a CSVWriter for Logging Purposes.
	 *
	 * @param fileName name of the csv file to write to
	 * @param keys     the csv file to write values to
	 * @param timer    a loggable timer
	 */
	public CsvWriter(String fileName, String[] keys, LogTimer timer) {

		this.timer = timer;

		File file = new File(fileName);
		try {
			//noinspection ResultOfMethodCallIgnored
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			fileWriter = new PrintWriter(file);
		} catch (IOException e) {
			e.printStackTrace();
		}

		StringBuilder line = new StringBuilder();

		currentFields.put("timestamp", timer.getTimestamp());
		line.append("timestamp");

		for (String k : keys) {

			if (k.equals("timestamp")) {
				throw new IllegalArgumentException("Cannot track a field called \"timestamp\"");
			}

			currentFields.put(k, null);
			if (line.length() != 0) {
				line.append(", ");
			}
			line.append(k);
		}

		writeLine(line.toString());
	}

	/**
	 * Updates all tracked values (if a tracked value is not
	 * given a new value, it will remain the same
	 * as when it was last tracked), and queues them to be written.
	 * Call this method to update fields
	 *
	 * @param newValues New log values
	 */
	public void update(HashMap<String, Object> newValues) {
		double currTime = timer.getTimestamp();
		currentFields.replace("timestamp", currTime);

		for (String key : newValues.keySet()) {
			if (!currentFields.containsKey(key)) {
				throw new IllegalArgumentException(
						String.format("Trying to log '%s', but logger doesn'timer have a column for it", key)
				);
			} else {
				currentFields.replace(key, newValues.get(key));
			}
		}

		queueCurrentLine();
	}

	/*
	 * Queue the current line of data
	 * @param time the time the data was recieved
	 */
	private void queueCurrentLine() {

		StringBuilder valsAsString = new StringBuilder();

		for (String k : currentFields.keySet()) {
			if (valsAsString.length() != 0) {
				valsAsString.append(", ");
			}
			valsAsString.append(currentFields.get(k));
		}

		linesToWrite.add(valsAsString.toString());
	}

	/**
	 * Goes through the queue of lines, and stores them in the file writer.
	 * Call this method to send the current fields to the file
	 */
	public void write() {
		while (true) {
			String dequedLine = linesToWrite.pollFirst();
			if (dequedLine == null) {
				break;
			}
			writeLine(dequedLine);
		}
	}

	/**
	 * Writes the given line to the CSV filewriter as entire row of a CSV file, and goes to the next
	 * line.
	 *
	 * @param line the line to write (delimited by ', ');
	 */
	private synchronized void writeLine(String line) {
		if (line != null) {
			this.fileWriter.write(line + "\n");
		}
	}


	/**
	 * {@code write}s the deque to the file, and then flushes everything stored in the file writer.
	 * (flushes the file writer is what actually updates the memory of the file)
	 */
	public synchronized void flushToFile() {
		if (fileWriter != null) {
			write();
			fileWriter.flush();
		}
	}

	/**
	 * Returns current fields.
	 *
	 * @return the current fields.
	 */
	public HashMap<String, Object> getCurrentFields() {
		return currentFields;
	}

	/**
	 * Returns the queue of lines to write.
	 *
	 * @return the queue of lines to write
	 */
	public ConcurrentLinkedDeque<String> getLinesToWrite() {
		return linesToWrite;
	}

}