package frc.team125.robot.utils;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * Class that makes it so that all types of controllers have the same buttons/controls and it's the same layout.
 */
public class Controller extends Joystick {
	private static final String F310_NAME = "Controller (Gamepad F310)", DUALSHOCK_NAME = "Wireless Controller", XBOX_NAME = "Controller (Xbox One For Windows)";
	private final SendableChooser<ControllerType> chooser = new SendableChooser<>();
	private volatile ControllerType type;
	private double leftStickXDeaband = 0, rightStickXDeadband = 0, leftStickYDeadband = 0, rightStickYDeadband = 0;

	/**
	 * Construct an instance of a joystick. The joystick index is the USB port on the drivers
	 * station.
	 *
	 * @param port The port on the Driver Station that the joystick is plugged into.
	 */
	public Controller(int port) {
		super(port);
		chooser.addOption("Auto", ControllerType.USE_NAME);
		chooser.addOption("Logitech F310", ControllerType.LOGITECH);
		chooser.addOption("Dualshock 4", ControllerType.PS4);
		chooser.addOption("Xbox Controller", ControllerType.XBOX);
		updateControllerType();
	}

	/**
	 * DISCLAIMER: At the start of every run mode controllers should be set
	 * Sets the controllers type based on the name that is given by the DriverStation
	 */
	public void updateControllerType() {
		if (usingShuffleBoardToSet())
			this.type = chooser.getSelected();
		else
			this.type = ControllerType.getTypeFromName(getName());
	}

	/**
	 * Sets the left stick X deadband
	 *
	 * @param deadband Deadband to set
	 * @return Controller
	 */
	public Controller setLeftStickXDeaband(double deadband) {
		this.leftStickXDeaband = deadband;
		return this;
	}

	/**
	 * Sets the right stick X deadband
	 *
	 * @param deadband Deadband to set
	 * @return Controller
	 */
	public Controller setRightStickXDeadband(double deadband) {
		this.rightStickXDeadband = deadband;
		return this;
	}

	/**
	 * Sets the left stick Y deadband
	 *
	 * @param leftStickYDeadband Deadband to set
	 * @return Controller
	 */
	public Controller setLeftStickYDeadband(double leftStickYDeadband) {
		this.leftStickYDeadband = leftStickYDeadband;
		return this;
	}

	/**
	 * Sets the right stick Y deadband
	 *
	 * @param rightStickYDeadband Deadband to set
	 * @return Controller
	 */
	public Controller setRightStickYDeadband(double rightStickYDeadband) {
		this.rightStickYDeadband = rightStickYDeadband;
		return this;
	}

	/**
	 * Gets the left and right trigger sum for driving. (RT - LT)
	 *
	 * @return Trigger sum
	 */
	public double getTriggerSum() {
		return getLeftTrigger() - getRightTrigger();
	}

	/**
	 * Gets the left joystick X value
	 *
	 * @return Left joystick X value
	 */
	public double getLeftStickX() {
		return Math.abs(getRawAxis(type.constants.leftX())) > leftStickXDeaband ? getRawAxis(type.constants.leftX()) : 0;
	}

	/**
	 * Gets the left joystick Y value
	 *
	 * @return Left joystick Y value
	 */
	public double getLeftStickY() {
		return Math.abs(getRawAxis(type.constants.leftY())) > leftStickYDeadband ? getRawAxis(type.constants.leftY()) : 0;
	}

	/**
	 * Gets the right joystick X value
	 *
	 * @return Right joystick X value
	 */
	public double getRightStickX() {
		return Math.abs(getRawAxis(type.constants.rightX())) > rightStickXDeadband ? getRawAxis(type.constants.rightX()) : 0;
	}

	/**
	 * Gets the Right joystick Y value
	 *
	 * @return Right joystick Y value
	 */
	public double getRightStickY() {
		return Math.abs(getRawAxis(type.constants.rightY())) > rightStickYDeadband ? getRawAxis(type.constants.rightY()) : 0;
	}

	/**
	 * Gets the left stick angle
	 *
	 * @return Left joystick angle
	 */
	public double getLeftStickDirection() {
		return Math.atan2(getLeftStickY(), getLeftStickX()) - Math.PI / 4;
	}

	/**
	 * Gets the right stick angle
	 *
	 * @return Right joystick angle
	 */
	public double getRightStickDirection() {
		return Math.atan2(getRightStickY(), getRightStickX()) - Math.PI / 4;
	}

	/**
	 * Gets the left stick magnitude
	 *
	 * @return Left stick magnitude
	 */
	public double getLeftStickMagnitude() {
		return Math.sqrt(Math.pow(getLeftStickX(), 2) + Math.pow(getLeftStickY(), 2));
	}

	/**
	 * Gets the right stick magnitude
	 *
	 * @return Right stick magnitude
	 */
	public double getRightStickMagnitude() {
		return Math.sqrt(Math.pow(getRightStickX(), 2) + Math.pow(getRightStickY(), 2));
	}

	/**
	 * Gets left stick press. It's the click when you press the joystick in
	 *
	 * @return Left stick press value
	 */
	public boolean getLeftStickPress() {
		return super.getRawButton(type.constants.l3());
	}

	/**
	 * Gets right stick press. It's the click when you press the joystick in
	 *
	 * @return Right stick press value
	 */
	public boolean getRightStickPress() {
		return super.getRawButton(type.constants.r3());
	}

	/**
	 * Gets the left bumper
	 *
	 * @return Left bumper value
	 */
	public boolean getLeftBumper() {
		return super.getRawButton(type.constants.lb());
	}

	/**
	 * Gets the right bumper
	 *
	 * @return Right bumper value
	 */
	public boolean getRightBumper() {
		return super.getRawButton(type.constants.rb());
	}

	/**
	 * Gets the left trigger
	 *
	 * @return Left trigger value
	 */
	public double getLeftTrigger() {
		return getRawAxis(type.constants.leftTrigger());
	}

	/**
	 * Gets the right trigger
	 *
	 * @return Right trigger
	 */
	public double getRightTrigger() {
		return getRawAxis(type.constants.rightTrigger());
	}

	/**
	 * Returns the bottom button on the right side of the controller
	 *
	 * @return Bottom button
	 */
	public boolean a() {
		return super.getRawButton(type.constants.a());
	}

	/**
	 * Returns the right button on the right side of the controller
	 *
	 * @return Right button
	 */
	public boolean b() {
		return super.getRawButton(type.constants.b());
	}

	/**
	 * Returns the left button on the right side of the controller
	 *
	 * @return Left button
	 */
	public boolean x() {
		return super.getRawButton(type.constants.x());
	}

	/**
	 * Returns the top button on the right side of the controller
	 *
	 * @return Top button
	 */
	public boolean y() {
		return super.getRawButton(type.constants.y());
	}

	/**
	 * Returns the start button
	 *
	 * @return Start button
	 */
	public boolean getStart() {
		return super.getRawButton(type.constants.start());
	}

	/**
	 * Returns the back button
	 *
	 * @return Back button
	 */
	public boolean getBack() {
		return super.getRawButton(type.constants.back());
	}

	/**
	 * Gets the dpad up value
	 *
	 * @return DPad up value
	 */
	public boolean getDPadUp() {
		int dPadValue = getPOV();
		var direction = 0;
		return (dPadValue == direction) || (dPadValue == (direction + 45) % 360)
				|| (dPadValue == (direction + 315) % 360);
	}

	/**
	 * Gets the dpad down value
	 *
	 * @return DPad down value
	 */
	public boolean getDPadDown() {
		int dPadValue = getPOV();
		var direction = 180;
		return (dPadValue == direction) || (dPadValue == (direction + 45) % 360)
				|| (dPadValue == (direction + 315) % 360);
	}

	/**
	 * Gets the dpad left value
	 *
	 * @return DPad left value
	 */
	public boolean getDPadLeft() {
		int dPadValue = getPOV();
		var direction = 270;
		return (dPadValue == direction) || (dPadValue == (direction + 45) % 360)
				|| (dPadValue == (direction + 315) % 360);

	}

	/**
	 * Gets the dpad right value
	 *
	 * @return DPad right value
	 */
	public boolean getDPadRight() {
		int dPadValue = getPOV();
		var direction = 90;
		return (dPadValue == direction) || (dPadValue == (direction + 45) % 360)
				|| (dPadValue == (direction + 315) % 360);
	}

	/**
	 * Rumbles both hands at a given percentage
	 *
	 * @param value Sets rumble percent [0, 1]
	 */
	public void rumble(double value) {
		rumbleLeft(value);
		rumbleRight(value);
	}

	/**
	 * Rumbles left hand at a given percentage
	 *
	 * @param value Sets rumble percent [0, 1]
	 */
	public void rumbleLeft(double value) {
		setRumble(RumbleType.kLeftRumble, value);
	}

	/**
	 * Rumbles right hand at a given percentage
	 *
	 * @param value Sets rumble percent [0, 1]
	 */
	public void rumbleRight(double value) {
		setRumble(RumbleType.kRightRumble, value);
	}

	@Override
	public boolean getRawButton(int id) {
		return super.getRawButton(type.constants.getButtonIDFromID(id));
	}

	@Override
	public boolean getRawButtonReleased(int id) {
		return super.getRawButtonReleased(type.constants.getButtonIDFromID(id));
	}

	@Override
	public boolean getRawButtonPressed(int id) {
		return super.getRawButtonPressed(type.constants.getButtonIDFromID(id));
	}

	/**
	 * Sends all necessary values to NetworkTables
	 */
	public void updateShuffleboard() {
		String driverOperator = getPort() == 0 ? "Driver" : "Operator";
		Shuffleboard.getTab("Controllers").add("Controller " + getPort() + " Type", this.chooser);
		SmartDashboard.putString(driverOperator + " Controller Type", this.type.name());
		SmartDashboard.putBoolean(driverOperator + " A", a());
		SmartDashboard.putNumber(driverOperator + " Right Trigger", getRightTrigger());
		SmartDashboard.putBoolean(driverOperator + " DPad Up", getDPadUp());
	}

	private boolean usingShuffleBoardToSet() {
		return chooser.getSelected() != ControllerType.USE_NAME;
	}


	private enum ControllerType {
		LOGITECH(new Constants()), PS4(new PS4Constants()), XBOX(new XBoxConstants()), USE_NAME(null);

		private final Constants constants;

		ControllerType(Constants constants) {
			this.constants = constants;
		}

		@SuppressWarnings("IfCanBeSwitch")
		static ControllerType getTypeFromName(String name) {
			if (name.equals(F310_NAME))
				return ControllerType.LOGITECH;
			else if (name.equals(DUALSHOCK_NAME))
				return ControllerType.PS4;
			else if (name.equals(XBOX_NAME))
				return ControllerType.XBOX;
			else
				return ControllerType.LOGITECH;
		}
	}

	/**
	 * The constants contain methods and not variables to make the inheritance work. And sorry for the length
	 */
	private static class Constants {
		int a() {
			return 1;
		}

		int b() {
			return 2;
		}

		int x() {
			return 3;
		}

		int y() {
			return 4;
		}

		int lb() {
			return 5;
		}

		int rb() {
			return 6;
		}

		int back() {
			return 7;
		}

		int start() {
			return 8;
		}

		int l3() {
			return 9;
		}

		int r3() {
			return 10;
		}

		int leftTrigger() {
			return 2;
		}

		int rightTrigger() {
			return 3;
		}

		int leftX() {
			return 0;
		}

		int leftY() {
			return 1;
		}

		int rightX() {
			return 4;
		}

		int rightY() {
			return 5;
		}

		//Don't ask
		int getButtonIDFromID(int id) {
			switch (id) {
				case 1:
					return a();
				case 2:
					return b();
				case 3:
					return x();
				case 4:
					return y();
				case 5:
					return lb();
				case 6:
					return rb();
				case 7:
					return back();
				case 8:
					return start();
				case 9:
					return l3();
				case 10:
					return r3();
				default:
					return 0;
			}
		}
	}

	//If these constants are wrong then decrement all the buttons
	private static class PS4Constants extends Constants {
		@Override
		int a() {
			return 2;
		}

		@Override
		int b() {
			return 3;
		}

		@Override
		int x() {
			return 1;
		}

		@Override
		int y() {
			return 4;
		}

		@Override
		int lb() {
			return 5;
		}

		@Override
		int rb() {
			return 6;
		}

		@Override
		int back() {
			return 9;
		}

		@Override
		int start() {
			return 10;
		}

		@Override
		int l3() {
			return 11;
		}

		@Override
		int r3() {
			return 12;
		}

		@Override
		int leftTrigger() {
			return 3;
		}

		@Override
		int rightTrigger() {
			return 4;
		}

		@Override
		int leftX() {
			return 0;
		}

		@Override
		int leftY() {
			return 1;
		}

		@Override
		int rightX() {
			return 2;
		}

		@Override
		int rightY() {
			return 5;
		}

		@Override
		int getButtonIDFromID(int id) {
			switch (id) {
				case 1:
					return a();
				case 2:
					return b();
				case 3:
					return x();
				case 4:
					return y();
				case 5:
					return lb();
				case 6:
					return rb();
				case 7:
					return back();
				case 8:
					return start();
				case 9:
					return l3();
				case 10:
					return r3();
				default:
					return 0;
			}
		}

		
/* Dualshock button map
		//Buttons
		public int A = 2;
		public int B = 3;
		public int X = 1;
		public int Y = 4;
		public int LB = 5;
		public int RB = 6;
		public int BACK = 9;
		public int START = 10;
		public int L3 = 11;
		public int R3 = 12;
		
		//Analog
		public int LEFT_TRIGGER = 3;
		public int RIGHT_TRIGGER = 4;
		public int LEFT_X = 0;
		public int LEFT_Y = 1;
		public int RIGHT_X = 2;
		public int RIGHT_Y = 5;
*/
	}

	private static class XBoxConstants extends Constants {
		int a() {
			return 1;
		}

		int b() {
			return 2;
		}

		int x() {
			return 3;
		}

		int y() {
			return 4;
		}

		int lb() {
			return 5;
		}

		int rb() {
			return 6;
		}

		int back() {
			return 7;
		}

		int start() {
			return 8;
		}

		int l3() {
			return 9;
		}

		int r3() {
			return 10;
		}

		int leftTrigger() {
			return 2;
		}

		int rightTrigger() {
			return 3;
		}

		int leftX() {
			return 0;
		}

		int leftY() {
			return 1;
		}

		int rightX() {
			return 4;
		}

		int rightY() {
			return 5;
		}
	}
}

