package frc.team125.robot.utils;

@SuppressWarnings("unused")
public class JoystickMap {
	// Logitech Controller
	public static final int A = 1;
	public static final int B = 2;
	public static final int X = 3;
	public static final int Y = 4;
	public static final int LB = 5;
	public static final int RB = 6;
	public static final int BACK = 7;
	public static final int START = 8;
	public static final int L3 = 9;
	public static final int R3 = 10;
	public static final int LEFT_TRIGGER = 2;
	public static final int RIGHT_TRIGGER = 3;
	public static final int LEFT_X = 0;
	public static final int LEFT_Y = 1;
	public static final int RIGHT_X = 4;
	public static final int RIGHT_Y = 5;
	// Launchpad Keys
	public static final String LZERO = "Zero";
	public static final String LFORCE_FRONT = "Force Front";
	public static final String LFORCE_BACK = "Force Back";
	public static final String LLOW_SCORE = "Low Rocket";
	public static final String LMID_SCORE = "Mid Rocket";
	public static final String LCS_FRONT = "Cargo Ship Front";
	public static final String LCS_BACK = "Cargo Ship Back";
	public static final String LHIGH_SCORE = "High Rocket";
	public static final String LDUNK = "Dunk";
	public static final String LGROUND_INTAKE = "Ground Intake";
	public static final String LFRONT_HATCH_INTAKE = "Front PS Hatch Intake";
	public static final String LFRONT_CARGO_INTAKE = "Front PS Cargo Intake";
	public static final String LBACK_HATCH_INTAKE = "Back PS Hatch Intake";
	public static final String LBACK_CARGO_INTAKE = "Back PS Cargo Intake";
	public static final String LSUPER_HATCH_INTAKE = "Super Hatch Intake";


}
