package frc.team125.robot.utils;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * This class creates an instance of a moving average.
 */
public class MovingAverage {
	private final int samples;

	private final Queue<Double> queue = new ArrayDeque<>();
	private double accumulator = 0;

	/**
	 * Constructor for the moving average.
	 *
	 * @param windowSeconds is the number of seconds averaging over
	 */
	public MovingAverage(double windowSeconds) {
		this.samples = (int) Math.ceil(windowSeconds / 0.02);

	}

	/**
	 * Updates the moving average with the current value of what's being averaged.
	 *
	 * @param value The value to be added to the queue.
	 */
	public void update(double value) {
		this.queue.add(value);
		this.accumulator += value;
		this.accumulator -= this.queue.remove();
	}

	/**
	 * Calculates the moving average.
	 *
	 * @return The current moving average
	 */
	public double get() {
		return this.accumulator / (double) samples;
	}

}
