package frc.team125.robot.utils;

/*
 * A interface to represent any class that can represent time for timestamps in the {@code
 * CSVWriter} class
 */
public interface LogTimer {
	double getTimestamp();
}
  