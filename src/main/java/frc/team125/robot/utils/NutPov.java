package frc.team125.robot.utils;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;

public class NutPov extends Button {
	private final Joystick joystick;
	private final int rangeHigh;
	private final int rangeLow;

	/**
	 * Constructor for a NutPov. Creates pov based on the high and low bounds.
	 *
	 * @param joy  The joystick the POV is on.
	 * @param high The high bound of the POV
	 * @param low  The low bound of the POV.
	 */
	public NutPov(Joystick joy, int high, int low) {
		this.joystick = joy;
		this.rangeHigh = high;
		this.rangeLow = low;
	}

	@Override
	public boolean get() {
		if (this.rangeLow == 0 && this.rangeHigh == 45) {
			return this.joystick.getPOV() < 45 && this.joystick.getPOV() >= 0;
		}
		return this.joystick.getPOV() > this.rangeLow && this.joystick.getPOV() < this.rangeHigh;
	}
}
