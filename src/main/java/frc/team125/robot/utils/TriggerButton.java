package frc.team125.robot.utils;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.Button;

public class TriggerButton extends Button {

	private static final double THRESHOLD = 0.75;

	private final Joystick joystick;
	private final int triggerAxis;
	private final double threshold;

	/**
	 * Constructor for a TriggerButton that takes in the configuration.
	 *
	 * @param joystick    The joystick the button is on.
	 * @param triggerAxis The axis that will now be a button.
	 * @param threshold   The threshold for how much of the trigger must be active for it to count as a button press.
	 */
	private TriggerButton(Joystick joystick, int triggerAxis, double threshold) {
		this.joystick = joystick;
		this.triggerAxis = triggerAxis;
		this.threshold = threshold;
	}

	/**
	 * Constructor that uses the default threshold value.
	 *
	 * @param joystick    The joystick the button is on.
	 * @param triggerAxis The acis that will now be a button
	 */
	public TriggerButton(Joystick joystick, int triggerAxis) {
		this(joystick, triggerAxis, THRESHOLD);
	}

	@Override
	public boolean get() {
		return this.joystick.getRawAxis(this.triggerAxis) > this.threshold;
	}

}

