package frc.team125.robot.utils;

/**
 * Thread class for periodically running something at a given interval
 */
public class PeriodicCaller extends Thread {
	private final Runnable runnable;
	private final long interval;
	private boolean stop = false;

	/**
	 * Constructs a new {@link PeriodicCaller} with a runnable to execute and an interval to iterate by
	 *
	 * @param intervalMillis Interval (in milliseconds) to iterate to call the runnable
	 * @param runnable       Runnable to execute
	 */
	public PeriodicCaller(long intervalMillis, Runnable runnable) {
		this.runnable = runnable;
		this.interval = intervalMillis;
	}

	public void run() {
		stop = false;
		while (!stop) {
			runnable.run();
			try {
				sleep(interval);
			} catch (InterruptedException ignored) {
			}
		}
	}

	/**
	 * Stops calling the runnable. I didn't want to use {@link Thread}'s interrupt because there was a higher risk of this class causing an err
	 */
	@Override
	public void interrupt() {
		stop = true;
	}
}
