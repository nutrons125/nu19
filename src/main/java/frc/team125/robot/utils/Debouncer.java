package frc.team125.robot.utils;

public class Debouncer {
	private final int minimumLoops;
	private int loopCounter = 0;

	public Debouncer(double minimumSeconds) {
		this.minimumLoops = (int) (Math.ceil(minimumSeconds / 0.02));
	}

	public boolean get() {
		return this.loopCounter > this.minimumLoops;
	}

	public void update(boolean conditional) {
		this.loopCounter = conditional ? loopCounter + 1 : 0;
	}
}
