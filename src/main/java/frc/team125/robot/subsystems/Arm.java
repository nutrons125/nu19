/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.team125.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.TalonSRXConfiguration;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.RobotMap;

public class Arm extends Subsystem {

	private final TalonSRX armMotor;
	private final double tolerance = 200; // TODO tune this
	private double currGoal = 0.0;
	private ArmState currState = ArmState.IDLE;
	private double outputPower = 0.0;

	/**
	 * Subsystem representing the telescoping arm part of the superstructure.
	 */
	Arm() {

		this.armMotor = new TalonSRX(RobotMap.ARM_MOTOR);

		TalonSRXConfiguration config = new TalonSRXConfiguration();
		config.peakOutputForward = Constants.Arm.MAX_POWER;
		config.peakOutputReverse = -Constants.Arm.MAX_POWER;
		config.nominalOutputForward = 0.0;
		config.nominalOutputReverse = 0.0;

		armMotor.configAllSettings(config);

		this.armMotor.setSensorPhase(true);

		this.armMotor.getSensorCollection().setQuadraturePosition(
				this.armMotor.getSensorCollection().getPulseWidthPosition(), 0);

		this.armMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 0);

		configMotionMagic(Constants.Arm.CRUISE_VELOCITY_UP, Constants.Arm.CRUISE_ACCEL_UP);

		this.armMotor.setNeutralMode(NeutralMode.Brake);
	}

	/**
	 * The main update method. This is called every time we want the robot to be updated. Should be
	 * called every update cycle of the robot.
	 */
	public void update() {

		switch (this.currState) {
			case DISABLED:
			case IDLE:
				this.outputPower = 0.0;
				this.runArm();
				break;
			case MAGIC:
				this.runMotionMagic();
				if (this.checkTermination()) {
					this.setHoldingPid();
					this.currState = ArmState.HOLDING;
				}
				break;
			case POSITION:
				this.runPosition();
				if (this.checkTermination()) {
					this.setHoldingPid();
					this.currState = ArmState.HOLDING;
				}
				break;
			case MANUAL:
				runArm();
				break;
			case HOLDING:
				holdPosition();
				break;
			default:
				break;
		}

	}

	/**
	 * Runs the arm to a goal using motion magic on the TalonSRXs.
	 */
	private void runMotionMagic() {
		this.armMotor.set(ControlMode.MotionMagic, this.currGoal);
	}

	//////////////////////////////
	// ALL THE MOVEMENT METHODS //
	//////////////////////////////

	/**
	 * Runs the arm to a specific position using PID on the TalonSRXs.
	 */
	private void runPosition() {
		this.armMotor.set(ControlMode.Position, this.currGoal);
	}

	/**
	 * Runs teh arm based on the output power set in setStaticOutput. Different from runArmManual.
	 * This should be used in the state machine, the other should be used for testing only.
	 */
	private void runArm() {
		armMotor.set(ControlMode.PercentOutput, this.outputPower);
	}

	/**
	 * Holds the arm at a specific position using the Position Closed Loop Control.
	 */
	private void holdPosition() {
		// Check to see if we're actually at position
		// If we're not at position, don't try to move, just hold the position
		this.setHoldingPid();
		this.runPosition();
	}

	/**
	 * Configures the velocity and acceleration for motion magic.
	 *
	 * @param cruiseVelocity     The max velocity of the system
	 * @param cruiseAcceleration The max acceleration of the system
	 */
	private void configMotionMagic(int cruiseVelocity, int cruiseAcceleration) {
		armMotor.configMotionCruiseVelocity(cruiseVelocity);
		armMotor.configMotionAcceleration(cruiseAcceleration);
	}


	/////////////////////////////////
	// ALL THE SETTERS AND CONFIGS //
	/////////////////////////////////

	/**
	 * Sets the goal, and the state of the arm to start the state machine.
	 *
	 * @param g The new goal position for the arm to go to
	 */
	void setMagicPosGoal(double g) {
		// TODO keep an eye on this fucking shit up later
		if (Math.abs(g - this.currGoal) <= this.tolerance * 2) {
			this.currState = ArmState.HOLDING;
			this.setHoldingPid();
		} else {
			if (g < this.currGoal) {
				// retracting
				this.setRetractingPid();
			} else if (g > this.currGoal) {
				// Extending
				this.setExtendingPid();
			}
			this.currState = ArmState.MAGIC;
		}

		this.currGoal = Math.max(Constants.ARM_TRUE_ZERO, g);
	}

	/**
	 * Sets the state of the arm manually.
	 *
	 * @param s Desired arm state.
	 */
	void setState(ArmState s) {
		this.currState = s;
	}

	/**
	 * Sets the new PID values for extending the arm.
	 */
	void setExtendingPid() {
		configMotionMagic(Constants.Arm.CRUISE_VELOCITY_UP, Constants.Arm.CRUISE_ACCEL_UP);
		// FULL OUTPUT IS 1023
		this.armMotor.config_kP(0, Constants.Arm.ARM_OUT_K_P, 0);
		this.armMotor.config_kI(0, Constants.Arm.ARM_OUT_K_I, 0);
		this.armMotor.config_kD(0, Constants.Arm.ARM_OUT_K_D, 0);
		this.armMotor.config_kF(0, 0.0, 0);
	}

	/**
	 * Sets the new PID values for retracting the arm.
	 */
	private void setRetractingPid() {
		configMotionMagic(Constants.Arm.CRUISE_VELOCITY_DOWN, Constants.Arm.CRUISE_ACCEL_DOWN);
		this.armMotor.config_kP(0, Constants.Arm.ARM_IN_K_P, 0);
		this.armMotor.config_kI(0, Constants.Arm.ARM_IN_K_I, 0);
		this.armMotor.config_kD(0, Constants.Arm.ARM_IN_K_D, 0);
		this.armMotor.config_kF(0, Constants.Arm.ARM_IN_K_F, 0);

	}

	/**
	 * Sets the new PID values for holding the arm.
	 */
	private void setHoldingPid() {
		configMotionMagic(Constants.Arm.CRUISE_VELOCITY_DOWN, Constants.Arm.CRUISE_ACCEL_DOWN);
		this.armMotor.config_kP(0, Constants.Arm.ARM_HOLDING_K_P, 0);
		this.armMotor.config_kI(0, Constants.Arm.ARM_HOLDING_K_I, 0);
		this.armMotor.config_kD(0, Constants.Arm.ARM_HOLDING_K_D, 0);
		this.armMotor.config_kF(0, Constants.Arm.ARM_HOLDING_K_F, 0);

	}

	/**
	 * Simply returns the current number of encoder clicks.
	 *
	 * @return The current number of encoder clicks.
	 */
	public int getEncoderClicks() {
		return (int) this.armMotor.getSelectedSensorPosition(0);
	}


	/////////////////////
	// ALL THE GETTERS //
	/////////////////////

	/**
	 * Calculates the length of arm extension in inches based on the current encoder position.
	 *
	 * @return The current length of extension.
	 */
	private double getLengthInches() {
		return (this.getEncoderClicks() - Constants.ARM_TRUE_ZERO) / Constants.CLICKS_PER_INCH;
	}

	/**
	 * Calculates whether or not the arm is at the correct position and if it's reay to move to the
	 * next part of the state machine.
	 *
	 * @return If arm is done moving/reached the termination point.
	 */
	private boolean checkTermination() {
		return Math.abs(this.getEncoderClicks() - this.currGoal) <= this.tolerance;
	}

	/**
	 * Considered done if the arm is in the HOLDING state.
	 *
	 * @return If the arm is done with it's set of motions.
	 */
	boolean isDone() {
		return this.currState == ArmState.HOLDING;
	}

	/**
	 * Updates the SmartDashboard with all the relevant drivetrain values.
	 */
	void updateSmartDashboard() {
		SmartDashboard.putNumber("Arm Position:", this.getEncoderClicks());
		SmartDashboard.putNumber("Arm Length:", this.getLengthInches());
		SmartDashboard.putNumber("Arm Feedback Error:", this.armMotor.getClosedLoopError());
		SmartDashboard.putNumber("Arm Goal:", this.currGoal);
		SmartDashboard.putString("Arm State:", this.currState.toString());
		SmartDashboard.putString("Arm Mode:", this.armMotor.getControlMode().toString());
		SmartDashboard.putNumber("Arm Current:", this.armMotor.getOutputCurrent());
		SmartDashboard.putNumber("Arm Output:", this.armMotor.getMotorOutputVoltage());
	}

	@Override
	public void initDefaultCommand() {
	}


	public enum ArmState {
		DISABLED, IDLE, MAGIC, MANUAL, HOLDING, POSITION
	}

}
