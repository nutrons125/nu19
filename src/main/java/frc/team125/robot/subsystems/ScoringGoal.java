package frc.team125.robot.subsystems;

import java.util.ArrayList;

/**
 * Class for a scoring goal, which is made up of SuperstructureGoals.
 */
public class ScoringGoal {

	private final ArrayList<SuperstructureGoal> goals = new ArrayList<>(0);

	/**
	 * Constructor for a ScoringGoal. Takes in all the possible goals as arguments.
	 *
	 * @param frontCargo      The score front cargo goal.
	 * @param backCargo       The score back cargo goal.
	 * @param frontHatch      The score front hatch goal.
	 * @param backHatch       The score back hatch goal.
	 * @param frontCargoReach The score front cargo reach goal.
	 * @param backCargoReach  The score back cargo reach goal.
	 * @param frontHatchReach The score front hatch reach goal.
	 * @param backHatchReach  The score back hatch reach goal.
	 */
	ScoringGoal(SuperstructureGoal frontCargo, SuperstructureGoal backCargo,
	            SuperstructureGoal frontHatch, SuperstructureGoal backHatch,
	            SuperstructureGoal frontCargoReach, SuperstructureGoal backCargoReach,
	            SuperstructureGoal frontHatchReach, SuperstructureGoal backHatchReach) {

		this.goals.add(backCargo);
		this.goals.add(backHatch);
		this.goals.add(backCargoReach);
		this.goals.add(backHatchReach);
		this.goals.add(frontCargo);
		this.goals.add(frontHatch);
		this.goals.add(frontCargoReach);
		this.goals.add(frontHatchReach);
	}

	/**
	 * Gets the corresponding superstructure scoring goal based off of the various starts of the
	 * robot.
	 *
	 * @param hatchState Whether the robot has a hatch or not (cargo).
	 * @param reach      If the robot is trying to hit a reach position or not (normal).
	 * @param frontSide  If the robot is scoring on the front side or not (back).
	 * @return The correct SuperstructureGoal.
	 */

	public SuperstructureGoal getGoal(boolean hatchState, boolean reach, boolean frontSide) {
		int total = 0;

		total += hatchState ? 1 : 0;
		total += reach ? 2 : 0;
		total += frontSide ? 4 : 0;

		return this.goals.get(total);
	}

}
