package frc.team125.robot.subsystems;

/**
 * Holds all the goals for the superstructure's individual subsystems.
 * Can then distribute the goals to the appropriate controllers.
 */
@SuppressWarnings("SameParameterValue")
public
class SuperstructureGoal {
	double pivotGoal;
	double armGoal;
	double wristGoal;
	boolean canBeReflected = false;
	boolean canReach = false;

	/**
	 * Constructor for the SuperstructureGoal.
	 *
	 * @param p The pivot goal.
	 * @param a The arm goal.
	 * @param w The wrist goal.
	 */
	public SuperstructureGoal(double p, double a, double w) {
		this.pivotGoal = Constants.Pivot.angleDegreesToEncoder(p);
		this.armGoal = a; // Constants.Arm.lengthToEncoder(a);
		this.wristGoal = Constants.Wrist.angleDegreesToEncoder(w);
	}

	/**
	 * Makes a SuperstructureGoal reflectable, smoothing out transitions between different goals.
	 *
	 * @param canBeReflected The reflection state of a SuperstructureGoal.
	 */
	void setReflection(boolean canBeReflected) {
		this.canBeReflected = canBeReflected;
	}

	/**
	 * Makes a scoring SuperstructureGoal reachable, which means it can extend out further than normal. Used
	 * for reaching over defensee when scoring.
	 *
	 * @param canReach The reachavle state of a SuperstructureGoal.
	 */
	void setReachable(boolean canReach) {
		this.canReach = canReach;
	}

	/**
	 * Determines if the two SuperstructureGoals are the same goal.
	 *
	 * @param other A different SuperstructureGoal to compare with.
	 * @return If they are the same goal
	 */
	boolean sameGoal(SuperstructureGoal other) {
		return this.pivotGoal == other.pivotGoal && this.armGoal == other.armGoal
				&& this.wristGoal == other.wristGoal;
	}
}
