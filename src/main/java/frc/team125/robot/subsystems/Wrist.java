/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.team125.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.TalonSRXConfiguration;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.RobotMap;
import frc.team125.robot.subsystems.IntakeGoal.WristIntakeState;
import frc.team125.robot.utils.Debouncer;

public class Wrist extends Subsystem {

	private final Timer lightTimer = new Timer();
	private final TalonSRX wristMotor;
	private final CANSparkMax intakeMotor;
	private final Solenoid intakeToggle;
	private final Debouncer intakeCurrentDebouncer = new Debouncer(0.1); // was .15
	private IntakeGoal.WristIntakeState currIntakeState;
	private ControlState currWristState;
	// Controls the state of the intake solenoid. True means intake closed for intake a hatch
	private boolean intakingHatch = true;
	private boolean hasGamePiece = false;
	private double currWristPosGoal = 0.0;
	private double currWristOutput = 0.0;

	/**
	 * Subsystem representing the wrist part of the superstructure.
	 */
	Wrist() {
		this.wristMotor = new TalonSRX(RobotMap.WRIST_MOTOR);
		this.intakeMotor = new CANSparkMax(RobotMap.INTAKE_MOTOR, MotorType.kBrushless);

		TalonSRXConfiguration wristConfig = new TalonSRXConfiguration();
		wristConfig.peakOutputForward = Constants.Wrist.MAX_POWER;
		wristConfig.peakOutputReverse = -Constants.Wrist.MAX_POWER;
		wristConfig.nominalOutputForward = 0.0;
		wristConfig.nominalOutputReverse = 0.0;

		this.wristMotor.configAllSettings(wristConfig);

		this.wristMotor.setInverted(true);
		this.wristMotor.setSensorPhase(true);
		this.wristMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, 0);
		this.wristMotor.getSensorCollection().setQuadraturePosition(
				this.wristMotor.getSensorCollection().getPulseWidthPosition(), 0);

		configMotionMagic(Constants.Wrist.CRUISE_VEL, Constants.Wrist.CRUISE_ACCEL);
		this.wristMotor.setNeutralMode(NeutralMode.Brake);

		// Configure the PID
		this.wristMotor.config_kP(0, Constants.Wrist.WRIST_K_P, 0);
		this.wristMotor.config_kI(0, Constants.Wrist.WRIST_K_I + 0, 0);
		this.wristMotor.config_kD(0, Constants.Wrist.WRIST_K_D, 0);
		this.wristMotor.config_kF(0, Constants.Wrist.WRIST_K_F, 0);

		// INITIALIZE THE SOLENOIDS AND WRIST STATE
		this.intakeToggle = new Solenoid(PneumaticsModuleType.CTREPCM, RobotMap.INTAKE_TOGGLE_SOLENOID);

		this.currIntakeState = IntakeGoal.WristIntakeState.IDLE;
		this.currWristState = ControlState.IDLE;

	}

	/**
	 * The main update function for the wrist where the state machine runs.
	 *
	 * @param hatch If we are we acquiring a hatch.
	 */
	public void update(boolean hatch) {
		this.intakingHatch = hatch;
		// Negative for hatches, positive for balls
		double hatchDirection = this.intakingHatch ? 1.0 : -1.0;
		double currIntakeOutput = this.currIntakeState.getPow(this.intakingHatch) * hatchDirection;

		SmartDashboard.putString("intake State", this.currIntakeState.toString());
		// State of the intake, not necessarily the wrist motion
		switch (this.currIntakeState) {
			case RUN_UNTIL:
				this.hasGamePiece = this.intakeCurrentDebouncer.get();
				if (this.hasGamePiece) {
					this.lightTimer.reset();
					this.lightTimer.start();
					this.currIntakeState = WristIntakeState.FLASHING;
				}
				break;
			case FLASHING:
				double flashingTime = 0.5;
				if (this.intakingHatch) {
					flashingTime = 1.0;
				}
				if (this.lightTimer.get() > flashingTime) {
					this.currIntakeState = WristIntakeState.CARRYING;
					this.lightTimer.stop();
				}
				//this.hasGamePiece = true;
				break;
			case CARRYING:
				this.hasGamePiece = true;
				// Nothing else to do here, motor run with intake state
				break;
			case SCORING:
				this.hasGamePiece = false;
				currIntakeOutput *= -1.0;
				// Nothing else to do here, motor run with intake state
				break;
			default:
				break;
		}

		// This is the logic for moving the wrist.
		switch (this.currWristState) {
			case DISABLED:
				this.setStaticOutput(0.0);
				runWrist();
				this.currIntakeState = WristIntakeState.IDLE;
				break;
			case CALIBRATING:
				if (this.isCalibrated()) {
					this.currWristState = ControlState.IDLE;
				}
				break;
			case IDLE:
				this.setStaticOutput(0.0);
				runWrist();
				break;
			case MAGIC:
				runMotionMagic();
				if (this.checkTermination()) {
					this.currWristState = ControlState.HOLDING;
				}
				break;
			case MANUAL:
				//runWrist();
				break;
			case HOLDING:
				holdPosition();
				break;
			default:
				break;
		}


		// SEts the intake solenoid and motor power here
		this.intakeMotor.set(currIntakeOutput);

		if (this.intakeToggle.get() == this.intakingHatch) {
			this.intakeToggle.set(!this.intakingHatch);
		}

    /*
    Kyle did a thing yay

    hatch     piston    desired
    true      true      false
    true      false     stay the same
    false     true      stay the same
    false     false     true

     */


		this.intakeCurrentDebouncer.update(Math.abs(this.intakeMotor.getOutputCurrent()) > 18);
	}

	/**
	 * Runs the wrist based on the output power set in setStaticOutput. Different from runWristManual.
	 * This should be used in the state machine, the other should be used for testing only.
	 */
	private void runWrist() {
		wristMotor.set(ControlMode.PercentOutput, this.currWristOutput);
	}

	//////////////////////////////
	// ALL THE MOVEMENT METHODS //
	//////////////////////////////

	/**
	 * Runs wrist to a goal using motion magic on the TalonSRXs.
	 */
	private void runMotionMagic() {
		//this.wristMotor.set(ControlMode.MotionMagic, this.currWristPosGoal);
		this.wristMotor.set(ControlMode.Position, this.currWristPosGoal);
	}

	/**
	 * Holds the pivot at a specific position using the Position Closed Loop Control.
	 */
	private void holdPosition() {
		// Check to see if we're actually at position
		// If we're not at position, don't try to move, just hold the position
		this.wristMotor.set(ControlMode.Position, this.currWristPosGoal);
	}

	/**
	 * Used to the set the goal for the motion magic OR position control (HOLDING).
	 *
	 * @param g The goal, given as a double representing encoder clicks/native CTRE units
	 */
	void setMagicPosGoal(double g) {
		this.currWristState = ControlState.MAGIC;
		this.currWristPosGoal = g;
	}

	/////////////////////////////////
	// ALL THE SETTERS AND CONFIGS //
	/////////////////////////////////

	/**
	 * Sets the state of the wrist.
	 *
	 * @param s Desired wrist state.
	 */
	public void setState(ControlState s) {
		this.currWristState = s;
	}

	/**
	 * This is used for setting a static output for the wrist. Will run the wrist at that power in the
	 * runWrist method (the MANUAL/IDLE state). This method should be called by the superstructure.
	 *
	 * @param pow Takes a power values, sets that to the current output.
	 */
	@SuppressWarnings("SameParameterValue")
	private void setStaticOutput(double pow) {
		this.currWristOutput = pow;
	}

	/**
	 * Configures the velocity and acceleration for motion magic.
	 *
	 * @param cruiseVelocity     The max velocity of the system (cruise velocity)
	 * @param cruiseAcceleration The max acceleration of the system (cruise accel)
	 */
	@SuppressWarnings("SameParameterValue")
	private void configMotionMagic(int cruiseVelocity, int cruiseAcceleration) {
		wristMotor.configMotionCruiseVelocity(cruiseVelocity);
		wristMotor.configMotionAcceleration(cruiseAcceleration);
	}

	/**
	 * Returns true if we currently have a game piece, hatch or cargo.
	 *
	 * @return True if game piece in intake
	 */
	boolean hasGamePiece() {
		return this.hasGamePiece;
	}

	/**
	 * Method that calculates the angle of the wrist given the current encoder position.
	 *
	 * @return The current angle in degrees.
	 */
	double getAngleDegrees() {
		return (Constants.WRIST_ZERO - this.getEncoderClicks()) / Constants.CLICKS_PER_DEG;
	}

	/////////////////////
	// ALL THE GETTERS //
	/////////////////////

	/**
	 * Simply returns the current number of encoder clicks.
	 *
	 * @return The current number of encoder clicks.
	 */
	public double getEncoderClicks() {
		return this.wristMotor.getSelectedSensorPosition();
	}

	/**
	 * Calculates whether or not the wrist is at its position and ready to move to the next part of
	 * the state machine.
	 *
	 * @return If the wrist is done moving or not.
	 */
	private boolean checkTermination() {
		// TODO tune this // Was 15
		double tolerance = 20;
		return Math.abs(this.getEncoderClicks() - this.currWristPosGoal) <= tolerance;
	}

	/**
	 * Considered done if the wrist is in the HOLDING state.
	 *
	 * @return If the arm is done with it's predetermined set of movements.
	 */
	boolean isDone() {
		return this.currWristState == ControlState.HOLDING;
	}

	/**
	 * Determines if the wrist has been calibrated. Will only start the state machine if it's in the
	 * proper starting position.
	 *
	 * @return Whether the robot is in the correct spot/calibrated.
	 */
	private boolean isCalibrated() {
		return (this.getEncoderClicks() < 0.0 && this.getEncoderClicks() > 0.0);
	}

	/**
	 * Used to find the current of the suction cup motor. This can be used to knowing when a game
	 * piece has been acquired.
	 *
	 * @return The current of the suction motor.
	 */
	private double getIntakeCurrent() {
		return this.intakeMotor.getOutputCurrent();
	}

	WristIntakeState getIntakeState() {
		return this.currIntakeState;
	}

	void setIntakeState(IntakeGoal.WristIntakeState s) {
		this.currIntakeState = s;
	}

	/**
	 * Prints necessary information to the smartdashboard.
	 */
	void updateSmartDashboard() {
		SmartDashboard.putNumber("Wrist Position", this.getEncoderClicks());
		SmartDashboard.putNumber("Wrist Angle:", this.getAngleDegrees());
		SmartDashboard.putNumber("Wrist Feedback Error:", this.wristMotor.getClosedLoopError());
		SmartDashboard.putNumber("Wrist Goal:", this.currWristPosGoal);
		SmartDashboard.putNumber("Wrist Output:", this.currWristOutput);
		SmartDashboard.putString("Wrist State:", this.currWristState.toString());
		SmartDashboard.putString("Wrist Mode:", this.wristMotor.getControlMode().toString());

		SmartDashboard.putBoolean("Hatch?", this.intakingHatch);
		SmartDashboard.putBoolean("Intake Solenoid Toggle", this.intakeToggle.get());
		SmartDashboard.putNumber("Intake Current", this.getIntakeCurrent());
		SmartDashboard.putBoolean("Has Game Piece?", this.hasGamePiece());
	}

	@Override
	public void initDefaultCommand() {
	}

	public enum ControlState {
		DISABLED, CALIBRATING, IDLE, MAGIC, MANUAL, HOLDING
	}

}
