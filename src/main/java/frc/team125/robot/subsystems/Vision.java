package frc.team125.robot.subsystems;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.RobotMap;

public class Vision extends Subsystem {
	private final double lightOn = 3.0;
	private final double lightOff = 1.0;
	private final NetworkTable frontCamFeed;
	private final NetworkTable rearCamFeed;
	private NetworkTable activeCamFeed;

	//Target Dependent
	private boolean usingFrontCam = false;

	private double frontLight;
	private double backLight;

	/**
	 * Gets the X offset in degrees.
	 */
	public Vision() {
		this.frontCamFeed = NetworkTableInstance.getDefault().getTable(RobotMap.LIMELIGHT_FRONT);
		this.rearCamFeed = NetworkTableInstance.getDefault().getTable(RobotMap.LIMELIGHT_BACK);
		this.setFrontCamFeed();
	}

	@Override
	public void initDefaultCommand() {
	}


	/**
	 * Gets the X offset in degrees.
	 *
	 * @return The X offset in degrees.
	 */
	public double getXOffset() {
		return activeCamFeed.getEntry("tx").getDouble(0.0);
	}

	/**
	 * Gets the Y offset in radians.
	 *
	 * @return The Y offset in radians.
	 */
	private double getYOffset() {
		return Math.toRadians(activeCamFeed.getEntry("ty").getDouble(0.0));
	}


	/**
	 * Gets the X limelight's latency.
	 *
	 * @return Latency
	 */
	private double getLatency() {
		return activeCamFeed.getEntry("tl").getDouble(0.0);
	}

	/**
	 * Returns 1 if a target is visible, returns 0 if there's no target visible.
	 *
	 * @return 1 or 0
	 */
	private double isTargetVisibleDouble() {
		return activeCamFeed.getEntry("tl").getDouble(0.0);
	}

	/**
	 * Gets the distance of the target using the formula from the limelight's documentation.
	 *
	 * @return The distance to the target using a formula.
	 */
	private double calculateDistanceFromCameraHeight(double targetHeight, double cameraHeight, double cameraAngle) {
		return (targetHeight - cameraHeight) / Math.tan(cameraAngle + getYOffset());
	}


	/**
	 * Gets the distance to the target based of off its area using a slope-intercept formula.
	 *
	 * @return The distance based off of the area of the target.
	 */
	private double calculateDistanceArea() {
		double slope = -56.4;
		double yintercept = 105.95;
		return slope * activeCamFeed.getEntry("ta").getDouble(0.0) + yintercept;
	}


	/**
	 * Sets the camera feed to stream from the front facing camera.
	 */
	public void setFrontCamFeed() {
		this.activeCamFeed = this.frontCamFeed;
		this.usingFrontCam = true;
		this.frontLight = this.lightOn;
		this.backLight = this.lightOff;
	}

	/**
	 * Sets the camera feed to the stream from the back facing camera.
	 */
	public void setRearCamFeed() {
		this.activeCamFeed = this.rearCamFeed;
		this.usingFrontCam = false;
		this.frontLight = this.lightOff;
		this.backLight = this.lightOn;
	}

	/**
	 * Makes the limelights flash.
	 */
	public void flash() {
		double lightFlash = 2.0;
		this.frontLight = lightFlash;
		this.backLight = lightFlash;
	}

	/**
	 * Updates the values of the Vision class.
	 */
	public void updateVisionDashboard() {
		SmartDashboard.putNumber("LimeLight Latency: ", getLatency());
		SmartDashboard.putBoolean("Target Found : ", isTargetVisibleDouble() == 1);
		SmartDashboard.putBoolean("Front Camera: ", this.usingFrontCam);
		this.frontCamFeed.getEntry("ledMode").setNumber(this.frontLight);
		this.rearCamFeed.getEntry("ledMode").setNumber(this.backLight);
	}
}