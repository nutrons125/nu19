/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.team125.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.TalonSRXConfiguration;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.RobotMap;


public class Pivot extends Subsystem {

	private final TalonSRX pivotMotor;
	private final double tolerance = 15; // TODO tune this
	private double currOutput = 0.0;
	private double currPosGoal = 0.0;
	private ControlState currState = ControlState.DISABLED;

	/**
	 * Subsystem representing the pivot part of the superstructure.
	 */
	Pivot() {
		this.pivotMotor = new TalonSRX(RobotMap.PIVOT_MOTOR);

		TalonSRXConfiguration pivotConfig = new TalonSRXConfiguration();
		pivotConfig.peakOutputForward = Constants.Pivot.MAX_POWER;
		pivotConfig.peakOutputReverse = -Constants.Pivot.MAX_POWER;

		pivotConfig.nominalOutputForward = 0.0;
		pivotConfig.nominalOutputReverse = 0.0;

		this.pivotMotor.configAllSettings(pivotConfig);

		this.pivotMotor.setInverted(true);
		this.pivotMotor.setSensorPhase(true);
		this.pivotMotor.getSensorCollection().setQuadraturePosition(
				this.pivotMotor.getSensorCollection().getPulseWidthPosition(), 0);
		this.pivotMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, 0);

		configMotionMagic(Constants.Pivot.CRUISE_VEL, Constants.Pivot.CRUISE_ACCEL);


		this.pivotMotor.config_kP(0, Constants.Pivot.PIVOT_K_P, 0);
		this.pivotMotor.config_kI(0, Constants.Pivot.PIVOT_K_I, 0);
		this.pivotMotor.config_kD(0, Constants.Pivot.PIVOT_K_D, 0);
		// Compute and update F term based on current position and direction that we're moving
		this.pivotMotor.config_kF(0, Constants.Pivot.PIVOT_K_F, 0);

		this.pivotMotor.setNeutralMode(NeutralMode.Brake);


	}

	/**
	 * The main update method. This is called every time we want the robot to be updated. Should be
	 * called every update cycle of the robot. Power to run pivot should be set with setStaticOutput,
	 * this will just run whatever that has been set to.
	 */
	public void update() {
		switch (this.currState) {
			case DISABLED:
			case IDLE:
				this.currOutput = 0.0;
				runPivot();
				break;
			case MAGIC:
				runMotionMagic();
				if (this.checkTermination()) {
					this.currState = ControlState.HOLDING;
				}
				break;
			case MANUAL:
				runPivot();
				break;
			case HOLDING:
				holdPosition();
				break;
			default:
				break;
		}
	}

	/**
	 * Runs the pivot based on the output power set in setStaticOutput. Different from runPivotManual.
	 * This should be used in the state machine, the other should be used for testing only.
	 */
	private void runPivot() {
		this.pivotMotor.set(ControlMode.PercentOutput, this.currOutput);
	}

	/////////////////////////////////
	// ALL OF THE MOVEMENT METHODS //
	/////////////////////////////////

	/**
	 * The method that runs the pivot using motion magic.
	 */
	private void runMotionMagic() {
    /* Set a different KP constant to hold the arm in place and fight gravity. It's essentially
    an additive KFF term but the TalonSRXs don't do additive KFF.
    */
		this.pivotMotor.set(ControlMode.MotionMagic, this.currPosGoal);
	}

	/**
	 * Holds the pivot at a specific position using the Position Closed Loop Control.
	 */
	private void holdPosition() {
		// Check to see if we're actually at position
		// If we're not at position, don't try to move, just hold the position
		this.pivotMotor.config_kP(0, 6.0, 0);
		this.pivotMotor.set(ControlMode.Position, this.currPosGoal);
	}

	private void configMotionMagic(int cruiseVelocity, int cruiseAcceleration) {
		pivotMotor.configMotionCruiseVelocity(cruiseVelocity);
		pivotMotor.configMotionAcceleration(cruiseAcceleration);
	}

	/////////////////////////////////
	// ALL THE SETTERS AND CONFIGS //
	/////////////////////////////////

	/**
	 * Sets the goal for the pivot, and the state for the state machine.
	 *
	 * @param g The goal of the pivot
	 */
	void setMagicPosGoal(double g) {
		// TODO revisit
		SmartDashboard.putNumber("Diff:", Math.abs(g - this.currPosGoal));
		if (Math.abs(g - this.currPosGoal) <= this.tolerance * 2) {
			this.currState = ControlState.HOLDING;
		} else {
			this.currState = ControlState.MAGIC;
		}
		this.currPosGoal = g;
	}

	/**
	 * Sets the state of the pivot.
	 *
	 * @param s Desired pivot state.
	 */
	void setState(ControlState s) {
		this.currState = s;
	}

	/**
	 * Sets the normal use PID constants for the pivot.
	 */
	void setConstants() {
		this.pivotMotor.config_kP(0, Constants.Pivot.PIVOT_K_P, 0);
		this.pivotMotor.config_kI(0, Constants.Pivot.PIVOT_K_I, 0);
		this.pivotMotor.config_kD(0, Constants.Pivot.PIVOT_K_D, 0);
	}

	/**
	 * Sets the reach use PID constants for the pivot.
	 */
	void setReachConstants() {
		this.configMotionMagic(Constants.Pivot.REACH_VEL, Constants.Pivot.REACH_ACCEL);
		this.pivotMotor.config_kP(0, Constants.Pivot.PIVOT_REACH_K_P, 0);
		this.pivotMotor.config_kD(0, Constants.Pivot.PIVOT_REACH_K_D, 0);
	}

	/**
	 * Gets the velocity of the pivot.
	 *
	 * @return The current velocity of the pivot in native units
	 */
	public double getVelocity() {
		return -this.pivotMotor.getSelectedSensorVelocity();
	}

	/////////////////////
	// ALL THE GETTERS //
	/////////////////////

	/**
	 * Checks to see if the pivot has reached its desired position and ready to move to the next part
	 * of the state machine.
	 *
	 * @return If the wrist is done moving or not.
	 */
	private boolean checkTermination() {
		return (Math.abs(this.getEncoderClicks() - this.currPosGoal) <= this.tolerance);
	}

	/**
	 * Pivot is considered done if it is in the HOLDING state.
	 *
	 * @return I fthe pivot is done with it's predetermined set of movements.
	 */
	boolean isDone() {
		return this.currState == ControlState.HOLDING;
	}

	/**
	 * Methods that calculates the angle of the wrist given the current encoder position.
	 *
	 * @return The current angle in degrees.
	 */
	double getAngleDegrees() {
		return (Constants.PIVOT_ZERO - this.getEncoderClicks()) / 11.4;
	}

	/**
	 * Simple returns the current number of encoder clicks/native CTRE units.
	 *
	 * @return The current number of clicks from the selected sensor position.
	 */
	public double getEncoderClicks() {
		return this.pivotMotor.getSelectedSensorPosition();
	}

	/**
	 * Updates the SmartDashboard with all the relevant drivetrain values.
	 */
	void updateSmartDashboard() {
		SmartDashboard.putNumber("Pivot Position:", this.getEncoderClicks());
		SmartDashboard.putNumber("Pivot Angle:", this.getAngleDegrees());
		SmartDashboard.putNumber("Pivot Feedback Error:", this.pivotMotor.getClosedLoopError());
		SmartDashboard.putNumber("Pivot Goal:", this.currPosGoal);
		SmartDashboard.putString("Pivot State:", this.currState.toString());
		SmartDashboard.putString("Pivot Mode:", this.pivotMotor.getControlMode().toString());
		SmartDashboard.putNumber("Pivot Current:", this.pivotMotor.getOutputCurrent());
		SmartDashboard.putNumber("Pivot Output:", this.pivotMotor.getMotorOutputVoltage());
	}

	@Override
	public void initDefaultCommand() {
	}


	public enum ControlState {
		DISABLED, IDLE, MAGIC, MANUAL, HOLDING
	}

}
