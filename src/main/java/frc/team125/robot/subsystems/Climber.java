package frc.team125.robot.subsystems;

import com.revrobotics.CANEncoder;
import com.revrobotics.CANPIDController;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.RobotMap;
import frc.team125.robot.utils.Debouncer;
import frc.team125.robot.utils.MovingAverage;

public class Climber extends Subsystem {

	//Motor stuff
	// Right side is the leader, left side is the follower.
	private final CANSparkMax climberRight = new CANSparkMax(RobotMap.CLIMBER_RIGHT,
			CANSparkMaxLowLevel.MotorType.kBrushless);
	private final CANSparkMax climberLeft = new CANSparkMax(RobotMap.CLIMBER_LEFT,
			CANSparkMaxLowLevel.MotorType.kBrushless);
	private final CANEncoder climberEncoder;
	private final Solenoid climberFingers = new Solenoid(PneumaticsModuleType.CTREPCM, RobotMap.CLIMBER_SOLENOID);
	private final Debouncer climbDebouncer = new Debouncer(0.1);
	private final MovingAverage climbMovingAverage = new MovingAverage(0.1);
	private final DigitalInput limitSwitch = new DigitalInput(0);
	private Boolean driveDown = true;
	private double power = 0;
	// Climber Member Variables
	private ClimberState currState = ClimberState.IDLE;

	/**
	 * Constructor for the climber. Initializes all of the spark controllers, and sets the default state of the
	 * climber fingers to be false so they don't trigger.
	 */
	Climber() {
		CANPIDController climberRightController = this.climberRight.getPIDController();
		climberRightController.setOutputRange(-Constants.Climber.MAX_OUTPUT,
				Constants.Climber.MAX_OUTPUT);

		CANPIDController climberLeftController = this.climberLeft.getPIDController();
		climberLeftController.setOutputRange(-Constants.Climber.MAX_OUTPUT,
				Constants.Climber.MAX_OUTPUT);

		this.climberEncoder = this.climberLeft.getEncoder();

		// The climber fingers are single use. Once they have been fired, they can't be unfired.
		this.climberFingers.set(false);
	}

	/**
	 * The main update method for the climber. Should be called every update cycle of the robot.
	 */
	// TODO CHECK ALL OF THIS PLEASE
	public void update() {

		switch (this.currState) {
			case IDLE:
			case STOP:
				//Climber does nothing
				this.power = 0.0;
				break;
			case CLIMBING:
				// Drive motors until you see the limit switch
				if (!this.limitSwitch.get()) {
					this.currState = ClimberState.HOLDING;
				}
				this.power = -0.7;

				break;
			case HOLDING:
				this.power = -0.05;

				break;
			case RETRACTING:
				this.power = 0.75;

				if (Math.abs(this.getCurrent()) > 20) {
					this.power = 0.0;
					this.currState = ClimberState.STOP;
				}
				break;
			case MANUAL:
				break;
			default:
				break;
		}

		this.climberRight.set(this.power);
		this.climberLeft.set(-1 * this.power);

		this.climbDebouncer.update(Math.abs(this.getCurrent()) > 20);
		this.climbMovingAverage.update(Math.abs(this.getCurrent()));
		//   && Math.abs(this.climberLeft.getOutputCurrent()) > 20);
	}

	/**
	 * Sets the climbing fingers in the correct position (down, up). Once set down, they must be
	 * manually reset.
	 */
	public void setFingers(boolean fingers) {
		if (!this.climberFingers.get()) {
			this.climberFingers.set(fingers);
		}
	}

	/**
	 * Sets the power/state of the climber based on the direction that the climber is moving (up vs
	 * down).
	 */
	public void setPower(boolean driveDown) {
		SmartDashboard.putNumber("manual climber:", power);
		this.driveDown = driveDown;

		if (this.driveDown && this.currState != ClimberState.HOLDING) {
			this.currState = ClimberState.CLIMBING;
			this.climberLeft.setOpenLoopRampRate(1);
			this.climberRight.setOpenLoopRampRate(1);
		} else if (!this.driveDown && this.currState != ClimberState.STOP) {
			this.currState = ClimberState.RETRACTING;
			this.climberLeft.setOpenLoopRampRate(0);
			this.climberRight.setOpenLoopRampRate(0);
		}

	}

	/////////////////////
	// ALL THE GETTERS //
	/////////////////////

	/**
	 * Overrides the current state of the climber.
	 *
	 * @param state The new state of the climber.
	 */
	public void setState(ClimberState state) {
		if (this.currState != ClimberState.HOLDING) {
			this.currState = state;
		}
	}

	public double getCurrent() {
		return this.climberRight.getOutputCurrent();
	}

	/**
	 * Updates the SmartDashboard with all the relevant climber values.
	 */
	void updateSmartDashboard() {
		SmartDashboard.putNumber("Climber Position", this.climberEncoder.getPosition());
		SmartDashboard.putString("Climber State:", this.currState.toString());
		SmartDashboard.putNumber("Climber R Current:", this.climberRight.getOutputCurrent());
		SmartDashboard.putNumber("Climber L Current:", this.climberLeft.getOutputCurrent());
		SmartDashboard.putNumber("Climber Output:", this.power);
		SmartDashboard.putBoolean("Limit Switch", this.limitSwitch.get());
		SmartDashboard.putBoolean("Climber Direction. Down?", this.driveDown);
		SmartDashboard.putNumber("Climber Moving Avg", this.climbMovingAverage.get());
	}

	@Override
	protected void initDefaultCommand() {

	}

	public enum ClimberState {
		IDLE, MANUAL, CLIMBING, HOLDING, RETRACTING, STOP
	}
}
