package frc.team125.robot.subsystems;

import com.kauailabs.navx.frc.AHRS;
import com.revrobotics.CANPIDController;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.RobotMap;

public class Drivetrain extends Subsystem {
	private static final double THROTTLE_CONSTANT = .30;
	private final AHRS gyro = new AHRS(I2C.Port.kMXP);
	private final CANSparkMax leftDriveMain = new CANSparkMax(RobotMap.LEFT_DRIVE_MAIN,
			MotorType.kBrushless);
	private final CANSparkMax leftDriveFollowerA = new CANSparkMax(RobotMap.LEFT_DRIVE_FOLLOWER_A,
			MotorType.kBrushless);
	private final CANSparkMax rightDriveMain = new CANSparkMax(RobotMap.RIGHT_DRIVE_MAIN,
			MotorType.kBrushless);
	private final CANSparkMax rightDriveFollowerA = new CANSparkMax(RobotMap.RIGHT_DRIVE_FOLLOWER_A,
			MotorType.kBrushless);

	/**
	 * Class for the robot drivetrain. Running 6 NEO motors per side, with SPARK Max Controllers
	 */
	public Drivetrain() {
		this.rightDriveMain.setInverted(true);
		this.rightDriveFollowerA.follow(rightDriveMain);
		this.leftDriveMain.setInverted(false);
		this.leftDriveFollowerA.follow(leftDriveMain);

		CANPIDController autoLController = this.leftDriveMain.getPIDController();
		CANPIDController autoRController = this.rightDriveMain.getPIDController();

		autoLController.setOutputRange(DrivetrainConstants.kMinOutput,
				DrivetrainConstants.kMaxOutput);
		autoRController.setOutputRange(DrivetrainConstants.kMinOutput,
				DrivetrainConstants.kMaxOutput);

		this.leftDriveMain.enableVoltageCompensation(12.0);
		this.rightDriveMain.enableVoltageCompensation(12.0);

		this.leftDriveMain.burnFlash();
		this.rightDriveMain.burnFlash();
		this.leftDriveFollowerA.burnFlash();
		this.rightDriveFollowerA.burnFlash();

		this.leftDriveMain.setSmartCurrentLimit(40);
		this.leftDriveFollowerA.setSmartCurrentLimit(40);

		this.rightDriveMain.setSmartCurrentLimit(40);
		this.rightDriveFollowerA.setSmartCurrentLimit(40);
	}


	/**
	 * The updata method for the drivetrain, that takes in input dictating the current state of the robot.
	 *
	 * @param climbing     Is the robot trying to climb
	 * @param assisted     Is the robot trying to vision align
	 * @param triggerSum   The current trigger sum value (the throttle)
	 * @param leftStick    The current left stick value (the turn)
	 * @param visionOffset The xOffset for vision
	 */
	public void update(boolean climbing, boolean assisted, double triggerSum, double leftStick,
	                   double visionOffset, boolean turningToAngle, double desiredAngle) {

		this.resetSparks();

		if (turningToAngle) {
			this.turnToAngle(desiredAngle);
			return;
		}

		if (assisted && !climbing) {
			// Assisted driving constants
			double max_offset = 23;
			if (Math.abs(visionOffset) < max_offset) {
				driveAssisted(triggerSum, visionOffset);
			} else if (Math.abs(visionOffset) > max_offset) {
				this.driveArcade(triggerSum, leftStick, 1.0, 0.7);
			} else {
				this.driveArcade(triggerSum, 0.0, 1.0, 0);
			}
		} else if (climbing) {
			this.driveArcade(triggerSum, leftStick, 0.5, 0.7);
		} else {
			pTrickDrive(triggerSum, leftStick, 1, .5, .7);
//            driveArcade(triggerSum, leftStick, 1.0, 0.7);
		}
	}

	private void turnToAngle(double desiredAngle) {
		double angleError = desiredAngle - this.getAngle();
		double turn = angleError * DrivetrainConstants.turning_kP;
		this.drive(turn, -turn);
		// SmartDashboard.putNumber("DT Angle Error", angleError);
		// SmartDashboard.putNumber("DT Desired Angle", desiredAngle);
	}


	private void resetSparks() {
		if (this.leftDriveFollowerA.getFault(CANSparkMax.FaultID.kHasReset)) {
			this.leftDriveMain.setInverted(false);
			this.leftDriveFollowerA.follow(this.leftDriveMain);
			this.leftDriveFollowerA.clearFaults();
			this.enableCoastMode();
		}

		if (this.rightDriveFollowerA.getFault(CANSparkMax.FaultID.kHasReset)) {
			this.rightDriveMain.setInverted(true);
			this.rightDriveFollowerA.follow(this.rightDriveMain);
			this.rightDriveFollowerA.clearFaults();
			this.enableCoastMode();
		}

	}


	/**
	 * Sets the SPARK Max Controllers to be in BrakeMode. Needed for driving in autonomous. Very hard
	 * to move robot when in BrakeMode, even when disabled.
	 */
	public void enableBrakeMode() {
		this.rightDriveMain.setIdleMode(IdleMode.kBrake);
		this.rightDriveFollowerA.setIdleMode(IdleMode.kBrake);
		this.leftDriveMain.setIdleMode(IdleMode.kBrake);
		this.leftDriveFollowerA.setIdleMode(IdleMode.kBrake);
	}

	/**
	 * Sets the SPARK Mac Controllers to be in CoastMode. Needed for driving in teleoperated. Much
	 * easier to move the robot when in CoastMode.
	 */
	public void enableCoastMode() {
		this.rightDriveMain.setIdleMode(IdleMode.kCoast);
		this.rightDriveFollowerA.setIdleMode(IdleMode.kCoast);
		this.leftDriveMain.setIdleMode(IdleMode.kCoast);
		this.leftDriveFollowerA.setIdleMode(IdleMode.kCoast);
	}

	private void drive(double powLeft, double powRight) {
		leftDriveMain.set(powLeft);
		rightDriveMain.set(powRight);
	}

	private void driveArcade(double throttle, double turn, double throttleScale, double turnScale) {
		double adjustedTurn = turn * turn * Math.signum(turn) * turnScale;
		throttle *= throttleScale;
		drive(throttle + adjustedTurn, throttle - adjustedTurn);
	}

	/**
	 * For P-Trick
	 * Inverse proportional driving (sort of)
	 *
	 * @param throttle          forward and backward input [-1, 1]
	 * @param turn              left and right input [-1, 1]
	 * @param throttleScale     scales throttle by a given scale factor [0, 1]
	 * @param lowestMaxTurnCmd  Sets the lowest max turn Cmd. If this is 0.2 then 1:throttle and 1:turn will make the turn output .2
	 * @param highestMaxTurnCmd Sets the highest max turn Cmd. If this is 0.8 then 0:throttle and 1:turn will make the turn output .8
	 */
	@SuppressWarnings("SameParameterValue")
	private void pTrickDrive(double throttle, double turn, final double throttleScale, final double lowestMaxTurnCmd, final double highestMaxTurnCmd) {
		final double slope = (highestMaxTurnCmd - lowestMaxTurnCmd) / -1;
		final double turnScale = (slope * Math.abs(throttle)) + highestMaxTurnCmd;

		throttle *= throttleScale;
		turn *= turnScale;

		driveArcade(throttle, turn, 1, 1);
	}

	/**
	 * Drives the robot in the assisted mode that uses vision to line up to the vision targets.
	 *
	 * @param throttle    The throttle value to apply to the drivetrain.
	 * @param angleOffset The offset that will be used for the PID loop.
	 */
	private void driveAssisted(double throttle, double angleOffset) {
		double adjustedThrottle;
		// if (distance <= MAX_DISTANCE && distance >= 45) {
		//   adjustedThrottle = (distance / MAX_DISTANCE) * throttle;
		// } else if (distance <= 45) {
		adjustedThrottle = THROTTLE_CONSTANT * throttle;
		// }
		double correction = angleOffset * DrivetrainConstants.assisted_kP
				- angleOffset * DrivetrainConstants.assisted_kD;
		drive(adjustedThrottle + correction, adjustedThrottle - correction);
	}

	public double getAngle() {
		return this.gyro.getAngle();
	}

	public void resetGyro() {
		this.gyro.reset();
	}

	@SuppressWarnings("unused")
	public double getLeftVel() {
		return leftDriveMain.getEncoder().getVelocity();
	}

	/**
	 * Updates the SmartDashboard with all the relevant drivetrain values.
	 */
	public void updateSmartDashboard() {
		SmartDashboard.putNumber("Right DT Enc Pos:", this.getRightEncoderPosition());
		SmartDashboard.putNumber("Left DT Enc Pos:", this.getLeftEncoderPosition());
		SmartDashboard.putNumber("DT Angle", this.getAngle());

		// Ticks/sec / (Ticks/rotation) (feet/rotation)
		SmartDashboard.putNumber("Right DT Output:", this.rightDriveMain.get());
		SmartDashboard.putNumber("Left DT Output:", this.leftDriveMain.get());


	}

	private double getRightEncoderPosition() {
		return -this.rightDriveMain.getEncoder().getPosition();
	}


	private double getLeftEncoderPosition() {
		return this.leftDriveMain.getEncoder().getPosition();
	}


	@Override
	protected void initDefaultCommand() {
		//setDefaultCommand(new ArcadeDriveCmd());
	}

	// Static class for handling the drivetrain PID stuff
	public static class DrivetrainConstants {

		static final double turning_kP = 0.015;
		// Vision PID constants
		static double assisted_kP = 0.01;
		static double assisted_kD = 0.00;

		// Autonomous PID constants
		static double kMaxOutput = 1;
		static double kMinOutput = -1;

	}
}
