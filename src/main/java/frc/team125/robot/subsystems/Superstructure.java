package frc.team125.robot.subsystems;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Superstructure extends Subsystem {


	private final Timer intakeTimer = new Timer(); // delay for the arm to fall
	private final Timer pistonTimer = new Timer(); // delay for the piston to come in
	private final CargoIntake cargoIntake;
	public Climber climber;
	public boolean acquiringHatch = true;
	public Arm arm;
	public Pivot pivot;
	public Wrist wrist;
	private IntakeSafety safety = IntakeSafety.IDLE;
	private SuperState currState;
	// Track the cargo intake state here
	private IntakeGoal.CargoIntakeGoal cargoGoal = IntakeGoal.CargoIntakeGoal.NONE;
	private SuperstructureGoal currGoal;

	/**
	 * The main superstructure of the robot. Controls the motion of the pivot and the arm together.
	 */
	public Superstructure() {
		this.arm = new Arm();
		this.pivot = new Pivot();
		this.wrist = new Wrist();
		this.cargoIntake = new CargoIntake();
		this.climber = new Climber();

		this.currState = SuperState.DISABLED;

		// Set the reflection state of the superstructure goals
		this.setProperties();
		this.initialize();
	}

	/**
	 * Used to set the active camera to the correct limelight.
	 *
	 * @return true if the pivot is angled forward.
	 */
	public boolean isArmForward() {
		return this.pivot.getAngleDegrees() >= 0;
	}

	public boolean justAcquiredGamePiece() {
		return this.wrist.hasGamePiece() && this.wrist.getIntakeState() == IntakeGoal.WristIntakeState.FLASHING;
	}

	/**
	 * The main update method. This calls the arm and pivot update methods as well. Handles all the
	 * state machine logic of the robot's superstructure.
	 */
	public void update() {

		// The Wrist will need to be handled separately
		switch (this.currState) {
			case DISABLED:
				this.pivot.setState(Pivot.ControlState.DISABLED);
				this.wrist.setState(Wrist.ControlState.DISABLED);
				this.arm.setState(Arm.ArmState.DISABLED);
				break;
			case EXTEND_SILLY:
				if (this.arm.isDone() && this.wrist.isDone()) {
					this.currState = SuperState.PIVOT_SILLY;
					this.pivot.setMagicPosGoal(this.currGoal.pivotGoal);
				}
				break;
			case PIVOT_SILLY:
				if (this.pivot.isDone()) {
					this.currState = SuperState.HOLDING;
				}
				break;
			case REACH:
				if (this.pivot.isDone() && this.arm.isDone() && this.pivot.isDone()) {
					this.currState = SuperState.HOLDING;
				}
				break;
			case MOVING_PIVOT:
				// The wrist will be holding when it's waiting to move (after intaking)
				// Or if it's finished it's motion
				if (this.pivot.isDone()) {
					this.currState = SuperState.EXTENDING;
					this.arm.setExtendingPid();
					this.arm.setMagicPosGoal(this.currGoal.armGoal);
					if (this.currGoal.sameGoal(Constants.CARRY) && this.wrist.hasGamePiece()) {
						this.pistonTimer.reset();
						this.pistonTimer.start();
						this.cargoIntake.extendPiston(true);

						this.wrist.setMagicPosGoal(this.currGoal.wristGoal);
					}
				}
				break;
			case RETRACTING:
				// Check the wrist position
				// Pivot holding at "old" goal
				if (this.arm.isDone()) {
					if (this.safety == IntakeSafety.FALLING) {
						if (this.intakeTimer.get() > 0.75 && this.wrist.isDone()) {
							this.currState = SuperState.MOVING_PIVOT;
							this.pivot.setMagicPosGoal(this.currGoal.pivotGoal);
							this.intakeTimer.stop();
						}
					} else {
						this.currState = SuperState.MOVING_PIVOT;
						this.pivot.setMagicPosGoal(this.currGoal.pivotGoal);
					}
				}
				break;
			case EXTENDING:
				// Pivot will hold at new goal

				//if (this.currGoal.sameObject(Constants.CARRY)) {
				//  this.cargoIntake.extendPiston(false);
				//}

				if (!this.currGoal.sameGoal(Constants.GROUND_CARGO_INTAKE)) {
					this.cargoIntake.extendPiston(false);
				}

				if (this.arm.isDone()) { //&& this.wrist.isDone()) {
					this.safety = IntakeSafety.IDLE;
					this.currState = SuperState.HOLDING;
				}
				break;
			case HOLDING:
				if (!this.currGoal.sameGoal(Constants.CARRY) || !this.currGoal.sameGoal(Constants.GROUND_CARGO_INTAKE)) {
					this.safety = IntakeSafety.IDLE;
				}

				break;
			case IDLE:
				this.pivot.setState(Pivot.ControlState.IDLE);
				this.arm.setState(Arm.ArmState.IDLE);
				this.wrist.setState(Wrist.ControlState.IDLE);

				break;
			default:

		}

		if (this.currGoal.sameGoal(Constants.GROUND_CARGO_INTAKE) && this.currState != SuperState.IDLE
				&& this.wrist.hasGamePiece()) {
			this.setGoal(Constants.CARRY);
		}


		switch (this.cargoGoal) {
			case NONE:
				this.cargoIntake.stopIntake();
				break;
			case RUN_FORWARD:
				this.cargoIntake.extendPiston(true);
				this.cargoIntake.runIntake();
				break;
			case RUN_REVERSE:
				this.cargoIntake.extendPiston(true);
				this.cargoIntake.reverseIntake();
				break;
			default:
		}

		// Wrist needs to run independently of the arm and pivot, but the arm should never extend
		// before the wrist has reached its position. Additionally, there will be some cases where the
		// wrist should move differently but we'll address those as they come up

		// If leaving the intake, we don't want to move the wrist quite yet. Instead we want to pivot
		// then move the wrist when "extending"


		this.pivot.update();
		this.arm.update();
		this.wrist.update(this.acquiringHatch);
		this.climber.update();
	}

	/**
	 * Sets all the goals of the superstructure, and sets the state machine into action.
	 *
	 * @param goal The new goal of the superstructure. Contains the individual arm and pivot goals.
	 */
	public void setGoal(SuperstructureGoal goal) {
		if (this.currGoal.sameGoal(goal)) {
			return;
		}
		SuperstructureGoal lastGoal = this.currGoal;
		this.currGoal = goal;

		if (!this.wrist.hasGamePiece() && !this.currGoal.sameGoal(Constants.GROUND_CARGO_INTAKE)) {
			this.wrist.setIntakeState(IntakeGoal.WristIntakeState.IDLE);
		}

		// The normal pivot constants, not the climbing ones
		this.pivot.setConstants();

		// If the last goal is reflectable and the new goal is also reflectable, then we can
		// move between the goals without having to retract the arm.
		// - Retract only if the arm goal of the new position is less than the old one. Then pivot.
		// - Pivot and then extend if the arm goal of the new position is greater than the old one.
		if (lastGoal.canBeReflected && this.currGoal.canBeReflected) {
			if (lastGoal.armGoal < this.currGoal.armGoal) {
				// Need to extend slightly
				this.currState = SuperState.MOVING_PIVOT;
				this.pivot.setMagicPosGoal(this.currGoal.pivotGoal); // Last pivot position
			} else if (lastGoal.armGoal >= this.currGoal.armGoal) {

				// Need to retract slightly
				this.currState = SuperState.RETRACTING;
				this.arm.setMagicPosGoal(this.currGoal.armGoal);
			}

			this.wrist.setMagicPosGoal(this.currGoal.wristGoal);
		} else if (lastGoal.sameGoal(Constants.ZERO) && this.currGoal.canBeReflected && !this.currGoal.canReach) {
			this.arm.setMagicPosGoal(this.currGoal.armGoal);
			this.wrist.setMagicPosGoal(this.currGoal.wristGoal);
			this.arm.setExtendingPid();
			this.currState = SuperState.EXTEND_SILLY;
		} else if (this.currGoal.canReach && lastGoal.canBeReflected) {
			this.pivot.setMagicPosGoal(this.currGoal.pivotGoal);
			this.arm.setMagicPosGoal(this.currGoal.armGoal);
			this.wrist.setMagicPosGoal(this.currGoal.wristGoal);
			this.arm.setExtendingPid();
			this.pivot.setReachConstants();
			this.currState = SuperState.REACH;
		} else {

			// If we're not reflecting between two goals that are reflectable, then we need to handle
			// the state machine transitions normally
			if (lastGoal.armGoal < Constants.ARM_SAFE_ZERO) {
				this.currState = SuperState.MOVING_PIVOT;
				this.pivot.setMagicPosGoal(this.currGoal.pivotGoal); // Last pivot position
			} else {
				// Always want to go to safe zero first
				this.arm.setMagicPosGoal(Constants.ARM_SAFE_ZERO); // Safe zero
				this.currState = SuperState.RETRACTING;
			}

			if (this.currGoal.sameGoal(Constants.GROUND_CARGO_INTAKE)) {
				this.intakeTimer.reset();
				this.intakeTimer.start();
				this.safety = IntakeSafety.FALLING;
				this.cargoGoal = IntakeGoal.CargoIntakeGoal.RUN_FORWARD;
			} else if (lastGoal.sameGoal(Constants.GROUND_CARGO_INTAKE)
					&& this.currGoal.pivotGoal < Constants.GROUND_CARGO_INTAKE.pivotGoal) {
				// Current goal should be less than cargo intake pivot goal because that's like 100ish and zero is straight up
				this.safety = IntakeSafety.RISING;
				this.pistonTimer.reset();
				this.pistonTimer.start();
				this.cargoGoal = IntakeGoal.CargoIntakeGoal.NONE;
			} else {
				this.cargoGoal = IntakeGoal.CargoIntakeGoal.NONE;
			}

			if (this.safety != IntakeSafety.RISING) {
				this.wrist.setMagicPosGoal(this.currGoal.wristGoal);
			}
		}

	}

	/**
	 * Sets the state of the wrist suction cup.
	 *
	 * @param state State of the wrist intake.
	 */
	public void setWristIntakeState(IntakeGoal.WristIntakeState state) {
		this.wrist.setIntakeState(state);
	}

	/**
	 * Prints the superstructure state to the smartdash. Also calls the arm and pivot smartdash
	 * methods.
	 */
	public void updateSmartDashboard() {
		this.arm.updateSmartDashboard();
		this.pivot.updateSmartDashboard();
		this.wrist.updateSmartDashboard();
		this.climber.updateSmartDashboard();

		SmartDashboard.putString("Superstructure State:", this.currState.toString());
		SmartDashboard.putBoolean("Extend intake", this.cargoIntake.getPiston());
	}

	/**
	 * This method initializes the superstructure and its state. Starts in idle mode. Can be used to
	 * "reset" the superstructure so it stops trying to pursue any goals.
	 */
	public void initialize() {
		this.currState = SuperState.IDLE;
		this.currGoal = new SuperstructureGoal(this.pivot.getAngleDegrees(),
				this.arm.getEncoderClicks(), this.wrist.getAngleDegrees());
	}

	/**
	 * Set the reflected state of all the goals that can be reflected. If a goal is reflected, it
	 * means it can move to any other reflected goal without having to retract the arm, just move
	 * pivot to the correct angle and also retract/extend the arm if needed Complementary scoring
	 * goals can be reflected: - Scoring high front / back with either cargo / hatch - Scoring mid
	 * front / back with either cargo / hatch - Scoring low DOES NOT need to be reflected
	 */
	private void setProperties() {
		Constants.SCORE_CARGO_MID_FRONT.setReflection(true);
		Constants.SCORE_CARGO_HIGH_FRONT.setReflection(true);
		Constants.SCORE_CARGO_MID_BACK.setReflection(true);
		Constants.SCORE_CARGO_HIGH_BACK.setReflection(true);

		Constants.SCORE_HATCH_MID_FRONT.setReflection(true);
		Constants.SCORE_HATCH_HIGH_FRONT.setReflection(true);
		Constants.SCORE_HATCH_MID_BACK.setReflection(true);
		Constants.SCORE_HATCH_HIGH_BACK.setReflection(true);

		Constants.REACH_CARGO_HIGH_FRONT.setReachable(true);
		Constants.REACH_CARGO_MID_FRONT.setReachable(true);
		Constants.REACH_HATCH_HIGH_FRONT.setReachable(true);
		Constants.REACH_HATCH_MID_FRONT.setReachable(true);
		Constants.REACH_CARGO_HIGH_BACK.setReachable(true);
		Constants.REACH_CARGO_MID_BACK.setReachable(true);
		Constants.REACH_HATCH_HIGH_BACK.setReachable(true);
		Constants.REACH_HATCH_MID_BACK.setReachable(true);
	}

	@Override
	protected void initDefaultCommand() {

	}

	public enum SuperState {
		MOVING_PIVOT, RETRACTING, EXTENDING, HOLDING, EXTEND_SILLY, PIVOT_SILLY, REACH, IDLE, DISABLED
	}

	public enum IntakeSafety {
		RISING, FALLING, IDLE
	}
}

