package frc.team125.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.team125.robot.RobotMap;


public class CargoIntake extends Subsystem {
	private static final double INTAKE_POW = 0.4;
	private final CANSparkMax intakeMotor = new CANSparkMax(RobotMap.CARGO_INTAKE_MOTOR,
			CANSparkMaxLowLevel.MotorType.kBrushless);
	private final Solenoid intakePiston = new Solenoid(PneumaticsModuleType.CTREPCM, RobotMap.CARGO_SOLENOID);

	/**
	 * The intake that picks the cargo up from the ground.
	 */
	CargoIntake() {
		this.intakeMotor.setInverted(true);

		this.intakePiston.set(false);
	}

	void runIntake() {
		intakeMotor.set(INTAKE_POW);
	}

	void reverseIntake() {
		intakeMotor.set(-INTAKE_POW);
	}

	void stopIntake() {
		intakeMotor.set(0);
	}

	/**
	 * Sets the state of the piston that extends the cargo intake.
	 *
	 * @param extend Should the cargo intake be extended.
	 */
	void extendPiston(boolean extend) {
		if (this.intakePiston.get() != extend) {
			this.intakePiston.set(extend);
		}
	}

	boolean getPiston() {
		return this.intakePiston.get();
	}


	@Override
	public void initDefaultCommand() {
		// Set the default command for a subsystem here.
	}
}
