package frc.team125.robot.subsystems;

public class Constants {

  /* The absolute positions for the superstructure
   Pivot down cargo side: 304
   Pivot down hatch side: 2642
   Arm all the way in: -765
   Arm all the way out: 27000
   Pivot straight up and down: 1790
   Arm at about 60 deg towards cargo: 971
   Arm at about 60 deg towards hatch: 1840

   Conversions:
   447 encoder clicks / cm
   434 = 90 - may be wrong
   2486 = -90 - may be wrong

   Wrist encoders:
   All the way back: 2900
   All the way forward: 4600
   "zero": 3980
   */

	// ZERO POSITIONS
	static final double PIVOT_ZERO = 745; //previous 1460
	static final double ARM_SAFE_ZERO = -2100;
	public static final SuperstructureGoal ZERO = new SuperstructureGoal(0, ARM_SAFE_ZERO, 0);
	public static final SuperstructureGoal CARGO_SHIP_BACK = new SuperstructureGoal(-6.4, ARM_SAFE_ZERO + 2318, -81.7);
	static final double ARM_TRUE_ZERO = -2757;
	public static final SuperstructureGoal HP_HATCH_INTAKE_FRONT = new SuperstructureGoal(84, ARM_TRUE_ZERO + 550, 0.0);
	public static final SuperstructureGoal HP_HATCH_INTAKE_BACK = new SuperstructureGoal(-84, ARM_TRUE_ZERO + 550, 0.0);
	// 9000
	//
	public static final SuperstructureGoal HP_SUPER_HATCH_INTAKE = new SuperstructureGoal(-84, ARM_TRUE_ZERO + 12284, 0.0);
	public static final SuperstructureGoal HP_CARGO_INTAKE_FRONT = new SuperstructureGoal(8.6, ARM_TRUE_ZERO + 2787, 74.4);
	public static final SuperstructureGoal HP_CARGO_INTAKE_BACK = new SuperstructureGoal(-8.6, ARM_TRUE_ZERO + 2786, -74.4);
	// 6527
	// Hatch Intake Actual: -1996
	// Super Hatch Actual: 9284
	public static final SuperstructureGoal SCORE_HATCH_LOW_FRONT = new SuperstructureGoal(84, ARM_TRUE_ZERO, 0);
	public static final SuperstructureGoal SCORE_HATCH_LOW_BACK = new SuperstructureGoal(-84, ARM_TRUE_ZERO, 0);
	public static final SuperstructureGoal SCORE_CARGO_LOW_FRONT = new SuperstructureGoal(60, ARM_TRUE_ZERO, 27.4);
	public static final SuperstructureGoal SCORE_CARGO_LOW_BACK = new SuperstructureGoal(-60, ARM_TRUE_ZERO, -27.4);
	public static final SuperstructureGoal GROUND_CARGO_INTAKE = new SuperstructureGoal(62, ARM_TRUE_ZERO, 107);
	public static final SuperstructureGoal BACK_GROUND_CARGO = new SuperstructureGoal(-117, ARM_TRUE_ZERO, 18);
	public static final SuperstructureGoal CARGO_SHIP_FRONT = new SuperstructureGoal(6.4, ARM_TRUE_ZERO + 2318, 81.7);
	public static final SuperstructureGoal PREP_CLIMBING = new SuperstructureGoal(-5, ARM_TRUE_ZERO + 2050, 75);
	static final double WRIST_ZERO = 1768;
	static final double CLICKS_PER_INCH = 1211.6; // (447 clicks / cm) * (2.54 cm / inch)
	static final double CLICKS_PER_DEG = 11.4;
	// STATIC POSITIONS
	// Reflected states are set in the Superstructure
	// Same as the PREP_GROUND_CARGO_INTAKE
	static final SuperstructureGoal CARRY = new SuperstructureGoal(20, ARM_SAFE_ZERO, 99);
	static final SuperstructureGoal SCORE_HATCH_MID_FRONT = new SuperstructureGoal(24, ARM_TRUE_ZERO + 6346, 63.5);
	static final SuperstructureGoal SCORE_HATCH_HIGH_FRONT = new SuperstructureGoal(11.3, ARM_TRUE_ZERO + 21522, 75.8);
	static final SuperstructureGoal SCORE_HATCH_MID_BACK = new SuperstructureGoal(-24, ARM_TRUE_ZERO + 6346, -63.5);
	static final SuperstructureGoal SCORE_HATCH_HIGH_BACK = new SuperstructureGoal(-11.3, ARM_TRUE_ZERO + 21522, -75.8);
	static final SuperstructureGoal SCORE_CARGO_MID_FRONT = new SuperstructureGoal(8.2, ARM_TRUE_ZERO + 10254, 79);
	static final SuperstructureGoal SCORE_CARGO_HIGH_FRONT = new SuperstructureGoal(3.5, ARM_TRUE_ZERO + 27121, 86.7);
	static final SuperstructureGoal SCORE_CARGO_MID_BACK = new SuperstructureGoal(-8.2, ARM_TRUE_ZERO + 10254, -79);
	static final SuperstructureGoal SCORE_CARGO_HIGH_BACK = new SuperstructureGoal(-3.5, ARM_TRUE_ZERO + 27121, -86.7);
	static final SuperstructureGoal REACH_CARGO_HIGH_FRONT = new SuperstructureGoal(21, ARM_TRUE_ZERO + 25860, 64.4);
	static final SuperstructureGoal REACH_HATCH_HIGH_FRONT = new SuperstructureGoal(23, ARM_TRUE_ZERO + 23300, 64.4);
	static final SuperstructureGoal REACH_CARGO_MID_FRONT = new SuperstructureGoal(34.6, ARM_TRUE_ZERO + 15182, 60);
	static final SuperstructureGoal REACH_HATCH_MID_FRONT = new SuperstructureGoal(38.1, ARM_TRUE_ZERO + 10862, 51.3);
	static final SuperstructureGoal REACH_CARGO_HIGH_BACK = new SuperstructureGoal(-21, ARM_TRUE_ZERO + 25860, -64.4);
	// -114 3
	static final SuperstructureGoal REACH_HATCH_HIGH_BACK = new SuperstructureGoal(-23, ARM_TRUE_ZERO + 23300, -64.4);
	public static final ScoringGoal HIGH_SCORING = new ScoringGoal(SCORE_CARGO_HIGH_FRONT,
			SCORE_CARGO_HIGH_BACK, SCORE_HATCH_HIGH_FRONT, SCORE_HATCH_HIGH_BACK, REACH_CARGO_HIGH_FRONT,
			REACH_CARGO_HIGH_BACK, REACH_HATCH_HIGH_FRONT, REACH_HATCH_HIGH_BACK);
	static final SuperstructureGoal REACH_CARGO_MID_BACK = new SuperstructureGoal(-34.6, ARM_TRUE_ZERO + 15182, -60);
	static final SuperstructureGoal REACH_HATCH_MID_BACK = new SuperstructureGoal(-38.1, ARM_TRUE_ZERO + 10862, -51.3);
	public static final ScoringGoal MID_SCORING = new ScoringGoal(SCORE_CARGO_MID_FRONT,
			SCORE_CARGO_MID_BACK, SCORE_HATCH_MID_FRONT, SCORE_HATCH_MID_BACK, REACH_CARGO_MID_FRONT,
			REACH_CARGO_MID_BACK, REACH_HATCH_MID_FRONT, REACH_HATCH_MID_BACK);
	// TODO get the actual values
	private static final SuperstructureGoal REACH_CARGO_SHIP_FRONT = new SuperstructureGoal(0, 0, 0);
	private static final SuperstructureGoal REACH_CARGO_SHIP_BACK = new SuperstructureGoal(0, 0, 0);
	@SuppressWarnings("unused")
	public static final ScoringGoal CARGO_SCORING = new ScoringGoal(CARGO_SHIP_FRONT,
			CARGO_SHIP_BACK, CARGO_SHIP_FRONT, CARGO_SHIP_BACK, REACH_CARGO_SHIP_FRONT, REACH_CARGO_SHIP_BACK,
			REACH_CARGO_SHIP_FRONT, REACH_CARGO_SHIP_BACK);


	static class Pivot {
		// PIVOT CONSTANTS
		static final double PIVOT_K_P = 11.0;
		static final double PIVOT_K_I = 0.0;
		static final double PIVOT_K_D = 300.0;
		static final double PIVOT_K_F = 0.0;


		// PIVOT CONSTANTS
		static final double MAX_POWER = 1.0;
		static final int CRUISE_VEL = 1000;
		static final int CRUISE_ACCEL = 600;
		static final int REACH_VEL = 500;
		static final int REACH_ACCEL = 300;
		static final double PIVOT_REACH_K_P = 30.0;
		static final double PIVOT_REACH_K_D = 400;

		/**
		 * Returns the number of encoder clicks/native CTRE units for an angle given in degrees. Angles
		 * are both positive and negative.
		 *
		 * @param angle The angle to be converted, given in degrees.
		 * @return The equivalent number of encoder clicks/native CTRE units.
		 */
		static double angleDegreesToEncoder(double angle) {
			return Constants.PIVOT_ZERO - (angle * CLICKS_PER_DEG);
		}
	}

	static class Arm {
		// ARM CONSTANTS
		static final double ARM_OUT_K_P = 2.0;
		static final double ARM_OUT_K_I = 0.0;
		static final double ARM_OUT_K_D = 0.0;

		static final double ARM_IN_K_P = 1.0;
		static final double ARM_IN_K_I = 0.0;
		static final double ARM_IN_K_D = 0.0;
		static final double ARM_IN_K_F = 0.0;

		static final double ARM_HOLDING_K_P = 0.5;
		static final double ARM_HOLDING_K_I = 0.0;
		static final double ARM_HOLDING_K_D = 0.0;
		static final double ARM_HOLDING_K_F = 0.0;

		static final double MAX_POWER = 1.0;

		// 220 cm / sec
		static final int CRUISE_VELOCITY_UP = 18000;
		static final int CRUISE_VELOCITY_DOWN = 18000;
		static final int CRUISE_ACCEL_UP = 13000;
		static final int CRUISE_ACCEL_DOWN = 8000;
	}

	static class Wrist {
		// PIVOT CONSTANTS
		static final double WRIST_K_P = 6.25;
		static final double WRIST_K_I = 0.00;
		static final double WRIST_K_D = 0.005;
		static final double WRIST_K_F = 0.0;

		static final double MAX_POWER = 1.0;

		static final int CRUISE_VEL = 0;
		static final int CRUISE_ACCEL = 0;

		/**
		 * Returns the number of encoder clicks/native CTRE units for an angle given in degrees. Angles
		 * are both positive and negative.
		 *
		 * @param angle The angle to be converted, given in degrees.
		 * @return The equivalent number of encoder clicks/native CTRE units.
		 */
		static double angleDegreesToEncoder(double angle) {
			return Constants.WRIST_ZERO - (angle * CLICKS_PER_DEG);
		}
	}

	static class Climber {
		static final double MAX_OUTPUT = 1.0;
	}
}
