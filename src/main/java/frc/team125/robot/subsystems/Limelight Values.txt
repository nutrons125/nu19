(Temporary)


Thresholding___________________

Hue
High: 82
Low: 50

Saturation
High: 255
Low: 233

Value
High: 200
Low: 126

Erosion Steps: 0

Dilation Steps: 1

Countour Filtering__________________
Sort Mode: Largest

Area
High: 0.0018
Low: 100

Fullness
High: 100
Low: 10

W/H Ratio
High: 20
Low: 0

Direction Filter: None

Target Grouping: Dual Target

Intersection Filter: None